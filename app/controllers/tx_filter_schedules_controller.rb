class TxFilterSchedulesController < ApplicationController
  before_action :set_tx_filter_schedule, only: [:show, :edit, :update, :destroy]

  ### Gon de los filtros
  #

  # GET /tx_filter_schedules
  # GET /tx_filter_schedules.json
  def index
    @tx_filter_schedules = TxFilterSchedule.all

  end

  # GET /tx_filter_schedules/1
  # GET /tx_filter_schedules/1.json
  def show
  end

  # GET /tx_filter_schedules/new
  def new
    @tx_filter_schedule = TxFilterSchedule.new
    @filter_id_schedule = params[:idFiltro];
    @display = 'none'

    # filtros = TxFilter.all
    # gon.filtros = filtros
    #
    # tx_sessions = TxSessions.all
    # gon.sesiones = tx_sessions
    #
    # tFormatos = Tformatos.all
    # gon.formatos = tFormatos
    #
    # tCampos = Tcamposformato.all
    # gon.tCamposFormatos = tCampos
    #
    # tLays = Tcamposlay.all
    # gon.tCamposLays = tLays
    # gon.false = true
    #
    # @@filter_id = params[:id]
    # if params[:id].present?
    #   @idFiltro = params[:id]
    #
    # end

  end

  # GET /tx_filter_schedules/1/edit
  def edit
    filtros = TxFilter.all
    gon.filtros = filtros


    tx_sessions = TxSessions.all
    gon.sesiones = tx_sessions

    tFormatos = Tformatos.all
    gon.formatos = tFormatos

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true

    if params[:id].present?
      @idHorario = params[:id]

      @tx_schedule_message = TxScheduleMessage.where(:schedule_id => @idHorario)

      @filter_id_schedule = @tx_filter_schedule.filter_id

      @numMsn = @tx_schedule_message.count

      if @numMsn >= 2
        @display = 'block'
      else
        @display = 'none'
      end
    end

  end

  # POST /tx_filter_schedules
  # POST /tx_filter_schedules.json
  def create

    ## Variables de los campos que no usan field_tag
    @limit = params[:limit]
    @optionLimit = params[:select_limit]

    if @optionLimit == '1'
      @max_tran = @limit
      @min_tran = 0
    elsif @optionLimit == '2'
      @max_tran = 0
      @min_tran = @limit
    end

    ## Valores de los campos tx_filter_schedule
    @Intermediate = params[:tx_filter_schedule][:Intermediate]
    @TimeOut = params[:tx_filter_schedule][:TimeOut]

    if @Intermediate == ""
      @Intermediate = 0
    end

    if @TimeOut == ""
      @TimeOut = 0
    end

    @tx_filter_schedule = TxFilterSchedule.new(tx_filter_schedule_params)
    @tx_filter_schedule.Max_Tran = @max_tran
    @tx_filter_schedule.Min_Tran = @min_tran
    @tx_filter_schedule.Intermediate = @Intermediate
    @tx_filter_schedule.TimeOut = @TimeOut
    @tx_filter_schedule.save

    if params[:msn].present?
      params[:msn].each do |key,obj|
        @tx_schedule_message = TxScheduleMessage.new(obj.permit(:Num_Alarma, :message, :bit_date, :bit_description, :bit_escala, :bit_limit, :bit_reference))
        @tx_schedule_message.schedule_id = @tx_filter_schedule.id
        @tx_schedule_message.save

      end
    end
  end

  # PATCH/PUT /tx_filter_schedules/1
  # PATCH/PUT /tx_filter_schedules/1.json
  def update

    @tx_filter_schedule

    ## Variables de los campos que no usan field_tag
    @limit = params[:limit]
    @optionLimit = params[:select_limit]

    if @optionLimit == '1'
      @max_tran = @limit
      @min_tran = 0
    elsif @optionLimit == '2'
      @max_tran = 0
      @min_tran = @limit
    end

    @tx_filter_schedule.Max_Tran = @max_tran
    @tx_filter_schedule.Min_Tran = @min_tran
    @tx_filter_schedule.update(tx_filter_schedule_params)

    if params[:msn].present?
      params[:msn].each do |key,obj|
        @key = key
        @idMsn = obj[:id]

        if @idMsn.present?
          @tx_schedule_message = TxScheduleMessage.find(@idMsn)
          @tx_schedule_message.update(obj.permit(:id, :Num_Alarma, :message, :bit_date, :bit_description, :bit_escala, :bit_limit, :bit_reference))
        else
          @tx_schedule_message = TxScheduleMessage.new(obj.permit(:Num_Alarma, :message, :bit_date, :bit_description, :bit_escala, :bit_limit, :bit_reference))
          @tx_schedule_message.schedule_id = @tx_filter_schedule.id
          @tx_schedule_message.save
        end
      end
    end
  end

  # DELETE /tx_filter_schedules/1
  # DELETE /tx_filter_schedules/1.json
  def destroy
    @@idFiltro =  @tx_filter_schedule.filter_id

    @tx_filter_schedule.destroy
    respond_to do |format|
      # format.html { redirect_to tx_filter_schedules_url, notice: 'Tx filter schedule was successfully destroyed.' }
      format.html { redirect_to new_tx_filter_detail_path(:id => @@idFiltro), notice: 'El horario fue eliminado con éxito' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_filter_schedule
      @tx_filter_schedule = TxFilterSchedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_filter_schedule_params
      params.require(:tx_filter_schedule).permit(:filter_id, :Hr_Start, :Hr_End, :Max_Tran, :Min_Tran, :Interval, :UMInterval, :Intermediate, :UMIntermediate, :TimeOut, :UMTimeout, :Accumulated_Amount, :Type_Evaluation_Amount)
    end
end
