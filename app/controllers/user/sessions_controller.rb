class User::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]
  skip_before_filter :verify_authenticity_token
  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create

    # @id_usuario = current_user.id
    # @id_aplicacion = 25
    # @idTipoReg = 14 #Login
    # @fecha = Time.now.strftime("%F")
    # @hora = Time.now.strftime("%k:%M:%S.00000")
    # @men = "Login"
    #
    #
    # @tbit = Tbitacora.new
    # @tbit.IdUsuario = @id_usuario
    # @tbit.IdAplicacion = @id_aplicacion
    # @tbit.IdTipoReg = @idTipoReg #Login
    # @tbit.Fecha = @fecha
    # @tbit.Hora = @hora


    # @valorEnvio = params[:valorEnvio]
    # @user = User.where("user_name = ? OR email= ?", @valorEnvio, @valorEnvio)
    # @user.each do |u|
    #   @cua = u.activo
    #   @u = u.name
    #   @d = u.last_name
    #   @b = @u + " " + @d
    # end
    # @tbit.Mensaje = @men + " " + @b
    # @tbit.save
    super
  end

  # DELETE /resource/sign_out
  def destroy
    puts 'LOGOUT'

    #---------- Guarda la información del usuario que va a salir de la aplicación

    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 16 #Logout
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Logout"

    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Logout
    @tbit.Fecha = @fecha
    @tbit.Hora = @hora


    @valorDest = params[:valorDest]
    @us = User.where("id = ? ", @id_usuario)
    @us.each do |bus|
      @un = bus.name
      @do = bus.last_name
      @busco = @un + " " + @do
     # puts 'buscando: ' + @busco.to_s
    end
    @tbit.Mensaje = @men + " " + @busco.to_s
    @tbit.save
    super
  end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_in_params
    devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  end
end
