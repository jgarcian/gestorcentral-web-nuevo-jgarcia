class TxScheduleReferencesController < ApplicationController
  before_action :set_tx_schedule_reference, only: [:show, :edit, :update, :destroy]

  # GET /tx_schedule_references
  # GET /tx_schedule_references.json
  def index
    @tx_schedule_references = TxScheduleReference.all
  end

  # GET /tx_schedule_references/1
  # GET /tx_schedule_references/1.json
  def show
  end

  # GET /tx_schedule_references/new
  def new
    @tx_schedule_reference = TxScheduleReference.new

    @@idSession = params[:idSession]

    @cp = TxScheduleReference.where(:session_id => params[:idSession]).as_json


      # gon.false = true

  #   @arrRefSe = []
  #
  #   if camposReferencia.present?
  #     camposReferencia.each do | datos |
  #       @arrRefSe.push(datos.field_formatId)
  #     end
  #   end
  end

  # GET /tx_schedule_references/1/edit
  def edit
  end

  # POST /tx_schedule_references
  # POST /tx_schedule_references.json
  def create
    # @tx_schedule_reference = TxScheduleReference.new(tx_schedule_reference_params)
    #

    @cmpRef = params[:arrCmpRe]

    ## Si existen elementos en el arreglo se borraran esos registros "id" que van en el array
    if @cmpRef.present?
      @arrCmpRef = @cmpRef.split(',')
      @arrCmpRef.each do | idCmpRefTable |
        @tx_schedule_reference = TxScheduleReference.find(idCmpRefTable)
        @tx_schedule_reference.destroy
      end
    end

    @camposRefUpdate = params[:tx_shcedule_reference]

    if @camposRefUpdate.present?
      @camposRefUpdate.each do |key, obj|
        @key = key
        @idCmpRefTab = obj[:id]

        if @idCmpRefTab.present?
          @tx_schedule_reference = TxScheduleReference.find(@idCmpRefTab)
          @tx_schedule_reference.update(obj.permit(:id, :field_formatId))
        else
          @tx_schedule_reference = TxScheduleReference.new(obj.permit(:field_formatId))
          @tx_schedule_reference.session_id = @@idSession
          @tx_schedule_reference.save
        end

      end
    end


    respond_to do |f|
      f.html { redirect_to tx_sessions_index_path, notice: 'Se editaron correctamente los campos' }
      f.js
    end
  end

  # PATCH/PUT /tx_schedule_references/1
  # PATCH/PUT /tx_schedule_references/1.json
  def update
    respond_to do |format|
      if @tx_schedule_reference.update(tx_schedule_reference_params)
        format.html { redirect_to @tx_schedule_reference, notice: 'Tx schedule reference was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_schedule_reference }
      else
        format.html { render :edit }
        format.json { render json: @tx_schedule_reference.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_schedule_references/1
  # DELETE /tx_schedule_references/1.json
  def destroy
    @tx_schedule_reference.destroy
    respond_to do |format|
      format.html { redirect_to tx_schedule_references_url, notice: 'Tx schedule reference was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def agregar_eliminar_cmpref

    puts "Voy a campos referencia"

    crear_campo = params[:crear_campo]
    eliminar_campo = params[:eliminar_campo]

    ## Variable del objeto para presentar en vista
    @tx_schedule_reference


    if eliminar_campo.present?
      id_registro = params[:id_registro]

      ## Eliminando Registro
      TxScheduleReference.find(id_registro).destroy

    end

    ## Crear un nuevo campo
    if crear_campo.present?
      id_campo = params[:id_campo]

      @tx_schedule_reference = TxScheduleReference.new
      @tx_schedule_reference.field_formatId = id_campo
      @tx_schedule_reference.key_id = current_user.id
      @tx_schedule_reference.session_id = @@idSession
      @tx_schedule_reference.save
    end

    ##Enviar valor a la vista

    render :json => @tx_schedule_reference

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_schedule_reference
      @tx_schedule_reference = TxScheduleReference.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_schedule_reference_params
      params.require(:tx_schedule_reference).permit(:schedule_id, :field_formatId, :key_id, :session_id)
    end
end
