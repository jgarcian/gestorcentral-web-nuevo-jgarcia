class TxFilterDetailsController < ApplicationController
  before_action :set_tx_filter_detail, only: [:show, :edit, :update, :destroy]

  # GET /tx_filter_details
  # GET /tx_filter_details.json
  def index
    @tx_filter_details = TxFilterDetail.all
  end

  # GET /tx_filter_details/1
  # GET /tx_filter_details/1.json
  def show
  end

  # GET /tx_filter_details/new
  def new
    @tx_filter_detail = TxFilterDetail.new

    ## Para pedir la ventana modal
    @tx_filter_schedule = TxFilterSchedule.new



    # Gon para traes los campos del formato y layout correspondiente
    @tx_sessions = TxSessions.all
    tx_sessions = TxSessions.all
    gon.sesiones = tx_sessions

    ## Gon para llenar selects Layout, Formato de ventana modal

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays


    ## Campos de Referencia
    camposRefCheck = TxScheduleReference.all
    gon.camposRef = camposRefCheck

    filtros = TxFilter.all
    gon.allFiltros = filtros

    gon.false = true


    @@filter_id = params[:id]

    if params[:id].present?
      @id = params[:id]
      @tx_filter = TxFilter.where(:Id => @id)
      @tx_filter_schedule = TxFilterSchedule.where(:filter_id => @id)
    end
  end

  # GET /tx_filter_details/1/edit
  def edit

    # Gon para traes los campos del formato y layout correspondiente
    @tx_sessions = TxSessions.all
    tx_sessions = TxSessions.all
    gon.sesiones = tx_sessions

    ## Gon para llenar selects Layout, Formato de ventana modal

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true
  end

  # POST /tx_filter_details
  # POST /tx_filter_details.json
  def create



    if ( params[:_method].present? && params[:_method] == 'create' )
      @tx_filter_detail = TxFilterDetail.new


    else
      respond_to do |format|


        ### Actualizar, reordenar, agrega y elimina "TODAS LAS condiciones del filtro".
        if params[:update_filter_detail] == '0'
          if !params[:update_filter_detail].nil?
            if ( params[:SoyNuevo] == 'true' && params[:ListarCondiciones] == 'true' )

              ## En esta variable obtengo todos los datos de las condiciones que vienen de un input en la vista new con id="filtroNew"
              @filtroCondiciones = params[:filtroUp].split(',')

              ## Esta variable guarda el total de condiciones que hay existentes en base y el valor es asignado por un script document.ready cuando se ejecuta la vista
              @total_condi_input = params[:total_condi_input]

              @cont = 0 # Contador para ir alamacenando los valores en diferentes variables
              @numC = 0 # Variable que es asiganada como numero de condición
              @updateCondiciones = false # Boleano para ejecutar un if que puede actualizar o crear condiciones


              ## Estas variables guardan los valores de cada condición
              @numberConUp
              @conectorUp
              @negadoUp
              @operadorUp
              @parentesisUp
              @valorUp
              @campoFormatoIdUp
              @posisionUp
              @lengthUp

              ## Itero los valores del arreglo "Condiciones", y voy guardando valor por valor en las variables antes creadas
              @filtroCondiciones.each do | datos |
                @cont += 1 ## Incremento el contador para guardar los valaores

                if( @cont == 1 )
                  @numberConUp = datos
                elsif ( @cont == 2)
                  @conectorUp = datos
                elsif ( @cont == 3 )
                  @negadoUp = datos
                elsif ( @cont == 4 )
                  @operadorUp =datos
                elsif ( @cont == 5 )
                  @parentesisUp = datos
                elsif ( @cont == 6 )
                  @valorUp = datos
                elsif ( @cont == 7 )
                  @campoFormatoIdUp = datos
                elsif ( @cont == 8 )
                  @posisionUp = datos
                elsif ( @cont == 9 )
                  @lengthUp = datos
                  @cont = 0 ######### Contador comienza desde cero
                  @updateCondiciones = true ####### Boleano que permite ejecutar el if para actualizar o crear las "Condiciones"
                end

                if @updateCondiciones
                  @updateCondiciones = false
                  @numC += 1 ### Esta variable reprenta el valor para el campo conditionnumber

                  ## Si esta existe esta condiciones con este id de filtro y número de condición se ejecuta el sig. if
                  @condicionesFiltro = TxFilterDetail.where(:filter_id => @@filter_id).where(:conditionnumber => @numC).present?

                  if @condicionesFiltro

                    ## Hago una consulta especifica solo a ese id de filtro y numero especifico de condicion para actualizar los datos
                    @upCons = TxFilterDetail.where(:filter_id => @@filter_id).where(:conditionnumber => @numC)
                    @upCons.each do |cd|
                      cd.conditionnumber = @numC
                      cd.connector = @conectorUp
                      cd.denied = @negadoUp
                      cd.operator = @operadorUp
                      cd.parenthesis = @parentesisUp
                      cd.value = @valorUp
                      cd.fieldFormat_id = @campoFormatoIdUp
                      cd.position = @posisionUp
                      cd.lenght = @lengthUp
                      cd.save
                    end

                  else

                    ## Si no se encontro ninguna condición con el número id de filtro y  número de condición se crea un nuevo elemento en la tabla "Condición"
                    @tx_filter_detail = TxFilterDetail.new
                    @tx_filter_detail.filter_id = @@filter_id
                    @tx_filter_detail.conditionnumber = @numC
                    @tx_filter_detail.connector = @conectorUp
                    @tx_filter_detail.denied = @negadoUp
                    @tx_filter_detail.operator = @operadorUp
                    @tx_filter_detail.parenthesis = @parentesisUp
                    @tx_filter_detail.value = @valorUp
                    @tx_filter_detail.fieldFormat_id = @campoFormatoIdUp
                    @tx_filter_detail.position = @posisionUp
                    @tx_filter_detail.lenght = @lengthUp
                    @tx_filter_detail.save
                  end
                end
              end
              ##### Finaliza la iteración de los valores de las condiciones.

              @nu = @numC.to_i ## Este es el valor de la ultima condición
              @to = @total_condi_input.to_i # Número de condiciones que existian al entrar a la vista new


               ## Las condiciones solo son eliminadas en vista - es decir hasta este punto todavia existen todas las condiciones en base y ninguna a sido eliminada todavia
              #  Al menos que el número de condicion sea menor al total de condiciones se ejecuta el sig. if para hacer un consulta y eliminar las ultimas condiciones ya que estas
              #  se repetian " a  consecuancia de iterar los valores del arreglo @filtroCondiciones"
              if @to > @nu
                @res = @to - @nu
                puts "##################################################### #{@res}"

                @desCondi = TxFilterDetail.where(:filter_id => @@filter_id).last(@res)
                @desCondi.each do |desCondi|
                  desCondi.destroy
                end
              end

            end
          end
        end

        ## Actualiza los datos del filtro, Descripción (Description), tipo de filtro (Order_number), color del filtro (Backgroundcolor).
        if params[:update_filter] == '0'
          if !params[:update_filter].nil?
            if ( params[:SoyNuevo] == 'true' )
              cero = false
              @tx_filter = TxFilter.where(:Id => @@filter_id)
            else
              @tx_filter_detail = TxFilterDetail.find(params[:id])
              cero = false
              @tx_filter = TxFilter.where(:Id => @tx_filter_detail.Session_id)
            end
            @description = params[:desFiltro]
            @typeFilter = params[:type_filter]
            @color = params[:Backgroundcolor]

            @tx_filter.each do | filtro|
              filtro.Description = params[:desFiltro]
              filtro.Order_number = params[:type_filter]

              if @typeFilter != '0'
                filtro.Backgroundcolor = @color
              else
                filtro.Backgroundcolor = nil
              end
              filtro.save
            end
          end
        end

        ## Actualizar los campos de referencia del filtro
        if params[:update_camposRef] == "0"
          if !params[:update_camposRef].nil?
            @cmpRef = params[:arrCmpRe]

            ## Si existen elementos en el arreglo se borraran esos registros "id" que van en el array
            if @cmpRef.present?
              @arrCmpRef = @cmpRef.split(',')
              @arrCmpRef.each do | idCmpRefTable |
                @tx_schedule_reference = TxScheduleReference.find(idCmpRefTable)
                @tx_schedule_reference.destroy
              end
            end

            @camposRefUpdate = params[:tx_shcedule_reference]

            if @camposRefUpdate.present?
              @camposRefUpdate.each do |key, obj|
                @key = key
                @idCmpRefTab = obj[:id]

                if @idCmpRefTab.present?
                  @tx_schedule_reference = TxScheduleReference.find(@idCmpRefTab)
                  @tx_schedule_reference.update(obj.permit(:id, :field_formatId))
                else
                  @tx_schedule_reference = TxScheduleReference.new(obj.permit(:field_formatId))
                  @tx_schedule_reference.Filter_id = @@filter_id
                  @tx_schedule_reference.save
                end

              end
            end

          end
        end

        if ( params[:SoyNuevo] == "true" )
          format.html { redirect_to new_tx_filter_detail_path(:id => @@filter_id), notice: t('notice.update_filter') }
          format.json { render :show, status: :created, location: @tx_filter_detail }
        else
          # if @tx_filter_detail.save //Este es el que estaba antes
          if @tx_filter_detail.present?
            format.html { redirect_to new_tx_filter_detail_path(:id => @tx_filter_detail.filter_id), notice: t('notice.update_filter') }
            format.json { render :show, status: :created, location: @tx_filter_detail }
          else
            format.html { render :new }
            format.json { render json: @tx_filter_detail.errors, status: :unprocessable_entity }
          end

        end
      end
    end
  end

  # PATCH/PUT /tx_filter_details/1
  # PATCH/PUT /tx_filter_details/1.json

  def update
    @conector = params[:tipoConector]
    @campo = params[:campo]
    @operador = params[:operator]
    @par = @tx_filter_detail.parenthesis


    if @operador == "8"
      @valDos = params[:valorDos]
      @valUno = params[:valor]

      @value = "{#{@valUno}}"+"{#{@valDos}}"
      @value

    else
      @value = params[:valor]
    end

    if @par != '0'
      @operador = '-1'
      @value = nil
      @campo = 0
    end

    @tx_filter_detail.denied = @negado
    @tx_filter_detail.connector = @conector
    @tx_filter_detail.fieldFormat_id = @campo
    @tx_filter_detail.operator = @operador
    @tx_filter_detail.value = @value

    respond_to do |format|
      if @tx_filter_detail.update(tx_filter_detail_params)
        @true = true
        # format.html { redirect_to @tx_filter_detail, notice: 'Tx filter detail was successfully updated.' }
        format.html { redirect_to action: :edit, notice: 'Tx filter detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_filter_detail }
      else
        format.html { render :edit }
        format.json { render json: @tx_filter_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_filter_details/1
  # DELETE /tx_filter_details/1.json
  def destroy


    @tx_filter_detail.destroy
    respond_to do |format|
      # format.html { redirect_to action: :index, notice: 'Tx filter detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_filter_detail
      @tx_filter_detail = TxFilterDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_filter_detail_params
      params.require(:tx_filter_detail).permit(:filter_id, :conditionnumber, :connector, :denied, :operator, :parenthesis, :value, :fieldFormat_id, :position, :lenght, :key_id)
    end
end
