class CorreoGrupoMailer < ApplicationMailer

  default :from => "krodriguez@kssoluciones.com.mx"

  def mandar_area_trabajo_group(state, obse, fraud, fecha, tiempo, emails, user, idTran, detail, reference, area, hist_alert, hist_trans)

    ########### Codigo Josue

    @detail = detail
    @reference = reference
    @area = area
    @hist_alert = hist_alert
    @hist_trans = hist_trans


    ########### Codigo Josue


    @user = user.name + " " + user.last_name
    @state = state
    @obse = obse
    @fraud = fraud
    @fecha = fecha
    @tiempo = tiempo
    @idTran = idTran
    @emails = emails

    #@id_usuario = current_user.id
    @id_usuario = user.id
    @id_aplicacion = 25
    @idTipoReg = 15
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Mail a Grupo de Trabajo"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg
    @tbit.Fecha = @fecha
    @tbit.Hora = @hora
    @tbit.Mensaje = @men
    @tbit.save

    @state.each do |state|
      @state_name = state.Descripcion
    end

    #Correos De Los Grupos
    cont = 0
    @emails.each do |emails|
      cont += 1
      if cont == 1
        @destinatarios = emails.correo
      else
        @destinatarios = @destinatarios + "," + emails.correo
      end
    end

    mail(:to => "#{@destinatarios}", :subject => default_i18n_subject(:user => user.name))

  end

  def mandar_area_trabajo_user(state, obse, fraud, fecha, tiempo, groups, emails, user, idTran, detail, reference, area, hist_alert, hist_trans)

    ########### Codigo Josue

    @detail = detail
    @reference = reference
    @area = area
    @hist_alert = hist_alert
    @hist_trans = hist_trans


    ########### Codigo Josue

    @state = state
    @obse = obse
    @fraud = fraud
    @fecha = fecha
    @tiempo = tiempo
    @emails = emails
    @gruops = groups
    @idTran = idTran
    @user_mail = user.email

    @id_usuario = user.id
    @id_aplicacion = 25
    @idTipoReg = 15
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Mail a Grupo Usuarios"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg
    @tbit.Fecha = @fecha
    @tbit.Hora = @hora
    @tbit.Mensaje = @men
    @tbit.save

    @state.each do |state|
      @state_name = state.Descripcion
    end

    #Nombre De Grupo
    @gruops.each do | name_group|
      @name_group = name_group.name
    end


    #Correos De Los Grupos
    cont = 0
    @emails.each do |mail|
      cont += 1
      if cont == 1
        @destinatariosMails = mail.correo
      else
        @destinatariosMails = @destinatariosMails + ", " + mail.correo
      end
    end

    mail to: @user_mail, subject: default_i18n_subject(user: user.name)
  end

  def cliente_tarjeta(name_state, obse_mail, alerta_Tran, customer_mail)

    name_state.each do |st|
      @estado = st.Descripcion
    end

    @observaciones = obse_mail
    @num_traj = alerta_Tran
    @email = customer_mail

    mail to: @email, subject: default_i18n_subject()
  end
end
