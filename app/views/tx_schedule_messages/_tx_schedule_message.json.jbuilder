json.extract! tx_schedule_message, :id, :schedule_id, :Num_Alarma, :letter_color, :background_color, :message, :bit_date, :bit_description, :bit_escala, :bit_limit, :bit_reference, :created_at, :updated_at
json.url tx_schedule_message_url(tx_schedule_message, format: :json)
