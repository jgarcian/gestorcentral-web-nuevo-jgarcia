json.extract! mail_group, :id, :group_id, :correo, :created_at, :updated_at
json.url mail_group_url(mail_group, format: :json)
