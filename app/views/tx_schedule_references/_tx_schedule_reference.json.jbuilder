json.extract! tx_schedule_reference, :id, :schedule_id, :field_formatId, :key_id, :session_id, :created_at, :updated_at
json.url tx_schedule_reference_url(tx_schedule_reference, format: :json)
