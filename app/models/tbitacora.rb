class Tbitacora < ApplicationRecord
  belongs_to :testadosxalerta, foreign_key: :IdEstado
  belongs_to :user, foreign_key: :IdUsuario
  belongs_to :tx_filters, foreign_key: :IdFiltro
  belongs_to :tgrupos, foreign_key: :IdGrupo
  belongs_to :tperfiles, foreign_key: :IdPerfil, primary_key: :IdPerfil
  belongs_to :tformatos, foreign_key: :IdFormato, primary_key: :IdFormato
  belongs_to :tlay, foreign_key: :IdLay, primary_key: :IdLay
  belongs_to :ttiporegalarma, foreign_key: :IdTipoReg, primary_key: :IdTipoReg
  has_many :tbitaobse
end
