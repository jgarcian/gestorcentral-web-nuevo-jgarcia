class TxSessions < ApplicationRecord
  # has_many :tx_filters, :pri ,:dependent => :destroy
  has_many :tx_filters, :foreign_key => "Session_id", :dependent => :destroy
  has_many :tx_schedule_references, :foreign_key => "session_id", :dependent => :destroy

end
