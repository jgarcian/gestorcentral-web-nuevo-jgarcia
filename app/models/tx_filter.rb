class TxFilter < ApplicationRecord
  belongs_to :tx_session
  has_many :tx_filter_details, :foreign_key => "id"
  has_many :tbitacora
  has_many :categori_filters
  has_many :tx_filter_details
  has_many :tx_schedule_references
  # has_many :tx_filter_schedules, :foreign_key => 'filter_id', :dependent => :destroy.
  #

  #belongs_to :filtering_categorization, foreign_key: :filtering_categorization_id
end
