console.log('Entre TxFilters');

//---Estilos para el wizard
$('.content').css('min-height','auto');
$('.content').css('height','auto');

//############################## SCRIPTS PARA EL METODO NEW ############//
//----Gon Para llenar Select Formatos
$('.selectLay').on('change', function(){
    $(".selectFormat option").remove();
    $(".selectFormat").attr('disabled', false);
    let traduSelectFormat = $('#traduSeleFormat').val();
    id_lay = $(this).val();


    $(".selectFormat").append($("<option value></option>").text(`${traduSelectFormat}`));
    for( f = 0; f < gon.formatos.length; f++ ){
        if( gon.formatos[f].IdLay == id_lay ){

            $(".selectFormat").append(`<option value="${gon.formatos[f].IdFormato}" name="optionFormat">${gon.formatos[f].Descripcion}</option>`)
        } else {
            $(".selectFormat option").remove();
            $(".selectFormat").attr('disabled', true);
            $(".selectFormat").append($("<option value></option>").text(`${traduSelectFormat}`));
        }
    }
});

//----Gon Para Llenar SelectFeild y Select Group
$('.selectFormat').on('change', function() {


    let traSelFeild = $("#traduSeleFeild").val();
    $(".selectFeild option").remove();
    $("#group_sel option").remove();
    id_format = $(this).val();
    // console.log(`valor de Layout: ${id_lay} y de Formato: ${id_format}`)

    $(".selectFeild").append($("<option value></option>").text(`${traSelFeild}`));
    for ( c = 0; c < gon.tCamposFormatos.length; c++ ){

        if( ( gon.tCamposFormatos[c].IdLay == id_lay ) && ( gon.tCamposFormatos[c].IdFormato == id_format ) ){

            for( d = 0; d < gon.tCamposLays.length; d++ ){

                if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){

                    if ( gon.tCamposLays[d].Alias != ""){

                        $(".selectFeild").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);
                        $("#group_sel").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);

                    } else{
                        $(".selectFeild").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`);
                        $("#group_sel").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`)
                    }

                }
            }

        }
    }

});

//----Ayuda a mostrar el select-chose en el wizard
setTimeout(function(){

    $('#group_sel_chosen').css('width','100%');
    var hijo = $('.search-field').children();

    hijo.css('width', '100%')

}, 2000);

//--Muestra input Main - General - Group
$('.filterSession').on('change', function(){
    var typeFilter = $(this).val();
    // alert("Entre changue");
    // console.log(option)

    if ( typeFilter === "1" ){

        $('.colorPicker').css('display', 'block');
        $('.choseSelect').css('display', 'none');
        $('.content').css('min-height','320px');
        $('.chosen-choices').html();

        //Limpar valores de los demas inputs

        $('.chosen-select').val([]).trigger('chosen:updated');

    } else if( typeFilter === "2" ) {

        $('.colorPicker').css('display', 'none');
        $('.choseSelect').css('display', 'block');
        $('.content').css('min-height','420px');

        //Limpar valores de los demas inputs
        $('.demo').val("");

    } else {

        $('.choseSelect').css('display', 'none');
        $('.colorPicker').css('display', 'none');
        $('.content').css('min-height','auto');

        //Limpar valores de los demas inputs
        $('.demo').val("");
        $('.chosen-select').val([]).trigger('chosen:updated');

    }
});

//----inicilizo chose-select
$(".chosen-select").chosen();


//---Crea la paleta de colores con jquery-minicolors-rails
$('.demo').each(function () {
    $(this).minicolors({
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        format: $(this).attr('data-format') || 'hex',
        keywords: $(this).attr('data-keywords') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'bottom left',
        swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
        change: function (value, opacity) {
            if (!value) return;
            if (opacity) value += ', ' + opacity;
            if (typeof console === 'object') {
                //console.log(value);
            }
        },
        theme: 'bootstrap'
    });
});

//---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
$('.operator_sel').change(function(){

    $(".betweenValue label").remove();
    $(".inNot label").remove();
    let operadorText = $('.operator_sel option:selected').text();
    let operadorVal = $('.operator_sel').val();

    //---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
    if ( operadorVal === "8" ){
        $('.betweenValue').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .inValue, .contDenied').css('display','none');
        $('.betweenValue').css('display','block');
        $('#valor, #selectList').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else if( operadorVal === "7" ){
        $('.inNot').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .betweenValue, .contDenied').css('display','none');
        $('.inValue').css('display','flex');
        $('#valor, .valOneBetween, .valTwoBetween').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else {

        $('.contDenied, .oneValue').css('display', 'block');
        $('.betweenValue, .inValue').css('display','none');
        $('.valOneBetween, .valTwoBetween, #selectList').val("");

    }

});


//---Script para el boton de carga archivos falso
var btnReal = document.getElementById('realBtnFile');

$('.falseBtnFile').on('click', function(){
    btnReal.click();
});

$('.realBtnFile').change(function(){
    var valFile = $(this).val()

    if ( valFile ){
        let txtFile = valFile.match( /[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
        $('.customText').html(txtFile);
        $('.customText').attr('value', txtFile);
        $('#selectList').fadeOut(100);
    } else {
        $('.customText').html(`${placeAddList}`);
        $('.customText').attr('value', '');
        $('#selectList').fadeIn(100);
    }
});

// Bloqueo select In
$('#selectList').on('change', function(){
    // alert('Entre list')
    let $_this = $(this).val();

    if( $_this !== "" ){
        $('.upFiles').fadeOut(100);
    } else {
        $('.upFiles').fadeIn(100);
    }

});


//---Radio Button i-checks
$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
});

//---Cambia el valor del input Denied
$('.icheckbox_square-green').on('ifClicked', function(){
    let _this = $('.icheckbox_square-green').hasClass('checked');

    if ( !_this ){

        $('#denied').val('1');

    } else {

        $('#denied').val('0');

    }
});

// var containers = $('#drag-elements').toArray();
// containers.concat($('.drop-target').toArray());

// let condiciones = $('#drag-elements');
//
// //---Drag & Drop Dragula
// let drake = dragula([condiciones[0]],{
//
//     // isContainer: function (el){
//     //     return el.classList.contains('drop-target');
//     // },
//     //
//     // copy: function (el, source){
//     //     return source === document.querySelector('#drag-elements')
//     // },
//     //
//     // accepts: function (el, target){
//     //     return target !== document.querySelector('#drag-elements')
//     // }
//
// });

let left = $('#drag-elements');

var drake =  dragula([left[0]],{
    revertOnSpill: true
});

function ordenFiltro(){
    let allCondi = $('#drag-elements').children(); // Todas las condiciones
    let btnAnd = `<span class="btn btn-primary btn-xs changeConector">${txtAnd}</span>`;

    $('#drag-elements tr.condicion').each(function(){
        let allIndex_Condiciones = $(this).index();  // Index de todas las tr con clase "condicion"
        // alert(allIndex_Condiciones);
        let indexConArriba = allIndex_Condiciones - 1;
        let queHayArriba = allCondi.eq(indexConArriba); // Selecionando en elementos que esta de bajo de parAbre
        // console.log(queHayArriba);

        if ( allIndex_Condiciones !== 0){

            if( queHayArriba.hasClass('condicion') || queHayArriba.hasClass('parCierre') ){

                if( $(this).children('td.conectorVal').html() === "-1"  ){
                    $(this).children('td.connector').html(btnAnd);
                    $(this).children('td.conectorVal').html('1');
                }

            } else if( queHayArriba.hasClass('parAbre') ){

                if( $(this).hasClass('condicion') ){
                    $(this).children('td.connector').html('');
                    $(this).children('td.conectorVal').html('-1');
                }
            }

        } else {
            $(this).children('td.connector').html('');
            $(this).children('td.conectorVal').html('-1');
        }

    });

    $('#drag-elements tr.parAbre').each(function(){
        let allIndex_Parentesis = $(this).index();
        let indexConArriba = allIndex_Parentesis- 1;
        let queHayArriba = allCondi.eq(indexConArriba);

        if( allIndex_Parentesis === 0 || queHayArriba.hasClass('parAbre')){

            $(this).children('td.connector').html('');
            $(this).children('td.conectorVal').html('-1');

        } else {

            if($(this).children('td.conectorVal').html() === '-1'){
                $(this).children('td.connector').html(btnAnd);
                $(this).children('td.conectorVal').html('1');
            }

        }

    });
};

// Handle Events
drake.on('drop', function(el, target, source, sibling){
    ordenFiltro();
});


//######## AGREGAR CONDICIONES Y VALIDACIONES PARA NO REPETIR CONDICIONES #######################
let arrayOneValue = [];

let arrayBetween = [];

let arrayList = [];

//---Número de Clase
let numCon = 0;

//---Efecto fade
let fadeIn = 1000;

function condicionIncompleta(){
    swal({
        title: `${sorrySwal}`,
        text: `${traduCondiIncompleta}`,
        type: "error"
    });
}

function condiExiste(){
    swal({
        title: `${sorrySwal}`,
        text: `${traduCondiExist}`,
        type: "error"
    });
}


// $('.addConditions').on('click', function(){
//
//     let conditionText;
//     let denegadoBetweenList;
//
//     //-- Texto de los Select
//     let campoText = $('.selectFeild option:selected').text();
//     let operadorText = $('.operator_sel option:selected').text();
//     let listText = $('#selectList option:selected').text();
//     let listUpload = $('#customText').text();
//     let connector =  $('#selectConnector option:selected').text();
//
//     //-- Valores de los Select
//     let campoVal = $('.selectFeild').val();
//     let operadorVal = $('.operator_sel').val();
//     let conectorVal = $('#selectConnector').val();
//
//     //-- Valores de Listas
//     let selectListVal = $('#selectList').val();
//     let fileUpload = $('#realBtnFile').val();
//
//     //-- Valores de los Inputs
//     let unValor = $('#valor').val();
//     let valOneBetween = $('.valOneBetween').val();
//     let valTwoBetween = $('.valTwoBetween').val();
//
//     //-- Check de Negado
//     let denied = $('#denied').val();
//
//     //---Agrega la palabra "NO" - menos Between / Not Between - In / Not In
//     if( denied === "1"){
//
//         negado = `${noPalabra}`;
//         tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;
//
//     } else if ( denied === "0" ) {
//
//         negado = "";
//         tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;
//
//     }
//
//     // if(connector === "Select Connector"){
//     //     conectorVal = -1;
//     //     connector = "-";
//     // }
//
//     //---GON TABLAS
//     let campos = gon.tCamposLays;
//
//
//
//     //-- Guardo la posision y longitud del campo selecionado en una td
//     for( let i = 0; i < campos.length; i++ ){
//
//         let campo = campos[i];
//
//         if( campo.IdCampo == campoVal ){
//
//             // alert(`El campo: ${campoVal} coincide con campo: ${campo.IdCampo}`);
//
//             td = `<td class="tdDispNone filterCondi" value="${campo.Posicion}" name="posision">${campo.Posicion}</td>
//                       <td class="tdDispNone filterCondi" value="${campo.Longitud}" name="longitud">${campo.Longitud}</td>`;
//         }
//
//     }
//
//     let numDeCondi = $('.tBodyCondition').children().length;
//     if( numDeCondi === 0 ){
//         conectorVal = -1;
//         // connector = "";
//         tdConector = `<td class="text-center connector"> </td>`;
//     } else {
//         tdConector = `<td class="text-center connector"> <span class="btn btn-primary btn-xs changeConector" title="Cambiar conector">${connector}</span></td>`;
//     }
//
//     tdEliminar = `<td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>`;
//
//     if ( "" !== unValor) {
//
//         numCon++;
//
//         //---Creo una variable donde concateno todos los valores
//         let conditionText = `${campoText} ${operadorText} ${unValor}`;
//
//
//         if( campoVal !== ""){
//             //value="${conditionText}" name="${numCon}" title="${operadorText}"
//             tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}">
//                       ${tdDenegado}
//                       ${tdConector}
//                       <td class="text-center expresion"><strong class="negado">${negado}</strong> <span class="campo" value="${campoVal}">${campoText}</span> <strong class="operador" value="${operadorVal}">${operadorText}</strong> <span class="valorUno" value="${unValor}">${unValor}</span> </td>
//                       ${tdEliminar}
//                       <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
//                       <td class="tdDispNone filterCondi denegadoVal" value="${denied}" name="denegado">${denied}</td>
//                       <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
//                       <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
//                       <td class="tdDispNone filterCondi" value="${unValor}" name="valor">${unValor}</td>
//                       <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
//                       ${td}
//                   </tr>`;
//         }
//
//         function agregarCondiOneVal(){
//
//             if( ( unValor !== "" && campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( unValor !== "" && campoVal !== "" && operadorVal !== "" )){
//
//                 let appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));
//
//                 $('.s_Condition').scrollTop( appCondition.offset().top );
//
//                 arrayOneValue.push(`${conditionText}`);
//
//                 console.log(arrayOneValue);
//
//                 //--Habilita select de los conectores
//                 $('#selectConnector').prop('disabled', false);
//
//             } else {
//                 condicionIncompleta();
//             }
//
//         }
//
//         if ( arrayOneValue.length !== 0 ){
//
//             let isDuplicateOne = false;
//
//             //---Recorro todos los valores almacenados en el arreglo para checar si ya existe la condición
//             for ( let i = 0; i < arrayOneValue.length; i++ ){
//
//                 //---- Esta variable itera todos los valores del arreglo
//                 let conditionArr = arrayOneValue[i];
//
//                 //---Camparo valores obtenidos de los inputs con los valores que ya existen en el arreglo
//                 if ( conditionText === conditionArr ){
//                     numCon--;
//
//                     condiExiste();
//
//                     isDuplicateOne = true;
//
//                 }
//             }
//
//             //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
//             if ( !isDuplicateOne ){
//                 agregarCondiOneVal()
//             }
//
//         } else{
//             agregarCondiOneVal()
//         }
//
//         //---Se limpian valores
//         $('.selectFeild').val("");
//         $('.operator_sel').val("");
//         $('#selectConnector').val("");
//         $('#valor').val("");
//         $('#denied').val('0');
//         $('.icheckbox_square-green').removeClass('checked');
//
//         ///-----Valido que los inputs de Between no esten vacios
//     }
//     else if ( valOneBetween !== "" && valTwoBetween !== "" ){
//
//         numCon++;
//
//         //---Creo Una variable donde concateno los valores Obtenidos de los inputs Between
//         conditionText = `${campoText} Between ${valOneBetween} AND ${valTwoBetween}`;
//
//         if( operadorText === " Between "){
//
//             denegadoBetweenList = 0;
//             negadoBetIn = "";
//             tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;
//
//         } else {
//
//             denegadoBetweenList = 1;
//             negadoBetIn = `${noPalabra}`;
//             tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;
//         }
//
//         //---Varibales para construir la Fila
//         // let parAbre = `<strong class="parAbre" value="1">${parentesis}</strong>`;
//         // let parCierre = `<strong class="parCierra" value="2">)</strong>`;
//         let negadoCampo = `<strong class="negado">${negadoBetIn}</strong>`;
//         let campo = `<span class="campo" value="${campoVal}">${campoText}</span>`;
//         let operador = `<strong class="operador">Between</strong>`;
//         // let menorQue = '<strong class="operador"><=</strong>';
//         let valorUno = `<span class="valorUno" value="${valOneBetween}">${valOneBetween}</span>`;
//         let valorDos = `<span class="valorDos" value="${valTwoBetween}">${valTwoBetween}</span>`;
//         // let conectorAnd = '<strong class="conector">AND</strong>';
//
//         if( campoVal !== ""){
//             tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}">
//                      ${tdDenegado}
//                      ${tdConector}
//                      <td class="text-center expresion">${negadoCampo} ${campo} ${operador} ${valorUno} <strong class="conector">${txtAnd}</strong> ${valorDos}</td>
//                      ${tdEliminar}
//                      <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
//                      <td class="tdDispNone filterCondi denegadoVal" value="${denegadoBetweenList}" name="denegado">${denegadoBetweenList}</td>
//                      <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
//                      <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
//                      <td class="tdDispNone filterCondi" value="{${valOneBetween}}{${valTwoBetween}}" name="valor">{${valOneBetween}}{${valTwoBetween}}</td>
//                      <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
//                      ${td}
//                   </tr>
//                   `;
//         }
//
//
//         function agregarCondiBetween(){
//
//             if( (campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( campoVal !== "" && operadorVal !== "" )){
//                 //---Imprime Fila
//                 $('.tBodyCondition').append($(tr).fadeIn(fadeIn));
//
//                 //--Habilita select de los conectores
//                 $('#selectConnector').prop('disabled', false);
//
//                 arrayBetween.push(`${conditionText}`);
//
//                 console.log(arrayBetween);
//
//             } else {
//                 condicionIncompleta();
//             }
//
//         }
//         //---Si hay elementos en el arreglo se ejecuta las sig. lineas
//         if ( arrayBetween !== 0){
//
//             let isDuplicateTwo = false;
//
//             //---Recorro todos los valores almacenados en el arreglo Between
//             for ( let i = 0; i < arrayBetween.length; i++ ){
//
//                 //---- Esta variable itera todos los valores del arreglo
//                 let conditionArr = arrayBetween[i];
//                 console.log(conditionArr);
//
//                 //---Si los valores obetenidos de Feild, Operatos e Inputs Between ya existen en el Arreglo se ejecuta las sig. lineas
//                 if ( conditionText == conditionArr ){
//                     numCon--;
//
//                     condiExiste();
//
//                     isDuplicateTwo = true;
//
//                 }
//             }
//
//             //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
//             if ( !isDuplicateTwo ){
//                 agregarCondiBetween()
//             }
//         }
//         else{
//             agregarCondiBetween()
//         }
//
//
//
//         //---Se limpian los valores
//         $('.selectFeild').val("");
//         $('.operator_sel').val("");
//         $('#selectConnector').val("");
//         $('.valOneBetween').val("");
//         $('.valTwoBetween').val("");
//         $('#denied').val('0');
//         $('.icheckbox_square-green').removeClass('checked');
//     }
//     else if ( selectListVal !== "" || fileUpload !== "" ) {
//
//         numCon++;
//
//         if ( selectListVal !== "" ){
//
//             var lista = `${listText}`,
//                 listVal = $('#selectList').val();
//
//             //---Creo una variable donde concateno todos los valores
//             conditionText = `${campoText} In ${listText}`;
//
//         } else {
//
//             var lista = `${listUpload}`,
//                 listVal = `${listUpload}`;
//
//             //---Creo una variable donde concateno todos los valores
//             conditionText = `${campoText} In ${listUpload}`;
//         }
//
//         if ( operadorText === " In "){
//
//             denegadoBetweenList = 0;
//             negadoBetIn = "";
//             tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;
//
//         } else {
//
//             denegadoBetweenList = 1;
//             negadoBetIn = "No";
//             tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;
//
//         }
//
//         if( campoVal !== ""){
//             tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}">
//                       ${tdDenegado}
//                       ${tdConector}
//                       <td class="text-center expresion"><strong class="negado">${negadoBetIn}</strong> <span class="campo" value="${campoVal}">${campoText}</span> <strong class="operador" value="${operadorVal}">In</strong> <span class="lista" value="${listVal}"> ${lista} </span> </td>
//                       ${tdEliminar}
//                       <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
//                       <td class="tdDispNone filterCondi denegadoVal" value="${denegadoBetweenList}" name="denegado">${denegadoBetweenList}</td>
//                       <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
//                       <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
//                       <td class="tdDispNone filterCondi" value="${lista}" name="valor">${lista}</td>
//                       <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
//                       ${td}
//                     </tr>`;
//         }
//
//
//         function agregarCondiList(){
//
//             if( ( campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( campoVal !== "" && operadorVal !== "" ) ){
//
//                 let appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));
//
//                 $('.s_Condition').scrollTop( appCondition.offset().top );
//
//                 //--Habilita select de los conectores
//                 $('#selectConnector').prop('disabled', false);
//
//                 arrayList.push(`${conditionText}`);
//
//                 console.log(arrayList);
//
//             } else {
//                 condicionIncompleta();
//             }
//         }
//
//         if ( arrayList.length !== 0 ){
//
//             var isDuplicateOne = false;
//
//             //---Recorro todos los valores almacenados en el arreglo para checar si ya existe la condición
//             for ( let i = 0; i < arrayList.length; i++ ){
//
//                 //---- Esta variable itera todos los valores del arreglo
//                 var conditionArr = arrayList[i];
//                 console.log(conditionArr);
//
//                 //---Camparo valores obtenidos de los inputs con los valores que ya existen en el arreglo
//                 if ( conditionText === conditionArr ){
//                     numCon--;
//
//                     condiExiste();
//
//                     isDuplicateOne = true;
//
//                 }
//             }
//
//             //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
//             if ( !isDuplicateOne ){
//                 agregarCondiList()
//             }
//
//         } else{
//             agregarCondiList()
//         }
//
//         $('.upFiles').fadeIn(100);
//         $('.selectFeild').val("");
//         $('.operator_sel').val("");
//         $('#selectConnector').val("");
//         $('#selectList').val("").fadeIn(100);
//         $('#realBtnFile').val("");
//         $('.customText').html(`${placeAddList}`);
//         $('#denied').val('0');
//         $('.icheckbox_square-green').removeClass('checked');
//
//     } else {
//         condicionIncompleta()
//     }
//
// });

//-- Eliminar Condiciones del arreglo
function eliminarCondicion(elemento, arrayConditions ){
    // alert("Entre Funcion borrar arreglo");
    // console.log(elemento);
    // console.log(arrayBetween);

    for ( let k in arrayConditions ){
        let condicion = arrayConditions[k];

        if ( elemento === condicion){
            // alert(` Estos ${elemento} es igual a esto ${condicion}`);

            let indice = arrayConditions.indexOf(condicion);

            arrayConditions.splice(indice, 1);

            console.log(arrayConditions);

            swal({
                title: "OK!",
                text: `${traduCondiDelete}`,
                type: "success"
            });

            let numCondi = $('.tBodyCondition').children().length;

            if( numCondi === 0 ){
                $('#selectConnector').prop('disabled', true);
            }

        }
    }

}


let colors = ['rgb(246, 229, 141,.5)', 'rgb(255, 190, 118,.5)', 'rgb(186, 220, 88,.5)', 'rgb(223, 249, 251,.5)', 'rgb(253, 121, 168,.5)', 'rgb(225, 112, 85,.5)'],
    indexColor = 0;

//-- Agregar bloque de Parenthesis
$('#selectParentesis').on('change', function(){
    // alert("Entre Parentesis");

    let longCondi = $('.tBodyCondition').children().length;
    let valorConector = $(this).val();
    let valNegado = $('#selectParentesis option:selected').attr('name')
    let conectorTxt = $('#selectParentesis option:selected').attr('title');
    let parText = $('#selectParentesis option:selected').text();


    //---Agrega la palabra "NO" - menos Between / Not Between - In / Not In
    if( valNegado === "1" ){

        negadoPar = `${noPalabra}`;
        tdDenegadoPar = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;

    } else {

        negadoPar = "";
        tdDenegadoPar = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

    }


    numCon++;

    if( longCondi !== 0 ){
        spanConector = `<span class="btn btn-primary btn-xs changeConector" title="Cambiar conector">${conectorTxt}</span>`;
    } else {
        spanConector = '';
    }

    let abrePartr = `<tr id="condicion${numCon}" class="parAbre" style="background-color: ${colors[indexColor]};">
                  ${tdDenegadoPar}
                  <td class="text-center connector"> ${spanConector}</td>
                  <td class="text-center expresion"><strong class="negado">${negadoPar}</strong> <span class="parenthesis">(</span></td>
                  <td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>
                  <td class="tdDispNone filterCondi conectorVal" value="${valorConector}" name="conector">${valorConector}</td>
                  <td class="tdDispNone filterCondi denegadoVal" value="${valNegado}" name="denegado">${valNegado}</td>
                  <td class="tdDispNone filterCondi" value="0" name="operador">0</td>
                  <td class="tdDispNone filterCondi" value="1" name="parantesis">1</td>
                  <td class="tdDispNone filterCondi" value="0" name="valor">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
              </tr>`;


    let cierrePartr = `<tr class="condicion${numCon} parCierre" style="background-color: ${colors[indexColor]};">
                              <td class="text-center enegado"></td>
                              <td class="text-center connector"></td>
                              <td class="text-center expresion parCierre"><strong class="parenthesis">)</strong></td>
                              <td class="text-center"></td>
                              <td class="tdDispNone filterCondi conectorVal" value="-1" name="conector">-1</td>
                              <td class="tdDispNone filterCondi denegadoVal" value="0" name="denegado">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="operador">0</td>
                              <td class="tdDispNone filterCondi" value="2" name="parantesis">2</td>
                              <td class="tdDispNone filterCondi" value="0" name="valor">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
                          </tr>`;

    if( longCondi !== 0 ) {
        // alert("Entre mayor");

        if( valorConector === "-1" ){
            swal({
                title: "ERROR!",
                text: `${noAddSinCon}`,
                type: "error"
            });

            numCon--;
            indexColor--;

        } else {

            $('.tBodyCondition').append($(abrePartr).fadeIn(fadeIn) );
            let appCondition = $('.tBodyCondition').append($(cierrePartr).fadeIn(fadeIn) );

            $('.s_Condition').scrollTop( appCondition.offset().top );

        }

    } else {

        if( valorConector === "0" || valorConector === "1"){

            swal({
                title: "ERROR!",
                text: `${noAddCone}`,
                type: "error"
            });

            numCon--;
            indexColor--;

        } else {

            $('.tBodyCondition').append($(abrePartr).fadeIn(fadeIn) );
            let appCondition = $('.tBodyCondition').append($(cierrePartr).fadeIn(fadeIn) );

            $('.s_Condition').scrollTop( appCondition.offset().top );

            //--Habilita select de los conectores
            $('#selectConnector').prop('disabled', false);
        }
    }

    $(this).val("");

    indexColor++;
    if( indexColor === 6){

        indexColor = 0;
    }

});


//--Cambiar Negado en la condicion
$('.tBodyCondition').on('click', 'input.changeDenied', function(){
    // alert("entre Conector");

    let tdPadre = $(this).parent();
    let tdDenegado = tdPadre.siblings('td.denegadoVal');
    let strongNo = tdPadre.siblings('td.expresion').find('strong.negado');
    // let strongNo = tdExpresion.children();

    if ( tdDenegado.html() === "0" ){
        tdDenegado.html('1');
        strongNo.html('No');
    } else {
        tdDenegado.html('0');
        strongNo.html('');
    }

});


//--Cambiar conector
$('.tBodyCondition').on('click', 'span.changeConector', function(){
    // alert("entre Conector");

    let tdPadre = $(this).parent();
    let tdConector = tdPadre.siblings('td.conectorVal');

    if ( tdConector.html() === "0" ){

        tdConector.html('1');
        $(this).html(`${txtAnd}`);

    } else {
        tdConector.html('0');
        $(this).html(`${txtOr}`);
    }

});

// Eliminar elementos del Filtro

// El evento click esta siendo detectado por el contenedor padre, pero este asu vez delega el evento a sus hijos en este caso di.clone
$('.tBodyCondition').on('click', 'span.eliminar', function() {
    // alert("Entre eliminar");

    let trPadre = $(this).parents('tr');
    let esPar = trPadre.hasClass('parAbre');
    let conEliminar = trPadre.attr('id');
    console.log(conEliminar);

    let value = trPadre.attr('value');
    let name = trPadre.attr('name');



    swal({
            title: `${swalDelete}`,
            text: `${dataWillLose}`,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: `${btn_yes}`,
            cancelButtonText: `${btn_cancel}`,
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {

            if (isConfirm){

                swal({
                        title: `${swalDelete}`,
                        text: `${yourSure}`,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: `${btn_confir}`,
                        cancelButtonText: `${btn_cancel2}`,
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {

                        if (isConfirm){


                            if( !esPar ){

                                $(`tr#${conEliminar}`).remove();

                                ordenFiltro();

                                if ( name === " Between " || name === " Not Between " ){

                                    eliminarCondicion(value, arrayBetween);

                                } else if ( name === " In " || name === " Not In " ){

                                    eliminarCondicion(value, arrayList);

                                } else {

                                    eliminarCondicion(value, arrayOneValue);

                                }

                            } else {

                                $(`tr#${conEliminar}`).remove();

                                $(`tr.${conEliminar}`).remove();

                                ordenFiltro();

                                swal({
                                    title: "OK!",
                                    text: `${traduCondiDelete}`,
                                    type: "success"
                                });

                                let numCondi = $('.tBodyCondition').children().length;

                                if( numCondi === 0 ){
                                    $('#selectConnector').prop('disabled', true);
                                }
                            }

                        }
                        else{
                            swal(`${titleCanel}`,`${txtCancel}`, "error");
                        }
                    });


            } else {
                swal(`${titleCanel}`,`${txtCancel}`, "error");
            }

        });

});

function detalleFiltro(){
    // Guardar filtro
    let valId = $('#userId').val();
    keyId = parseInt(valId);
    console.log(keyId);
    let valor = "";
    let td = $('.tBodyCondition').find('td.filterCondi');
    let contValore = 0;
    let indexConSave = 0;
    let addCondicion = false;

    td.each(function () {
        valor = $(this).html();
        contValore++;
        console.log(contValore);

        if (contValore === 1) {
            console.log(`Conector: ${valor}`);
            conectorVal = parseInt(valor);
        } else if (contValore === 2) {
            console.log(`Negado: ${valor}`);
            negadoVal = parseInt(valor);
        } else if (contValore === 3) {
            console.log(`Operador: ${valor}`);
            opeVal = parseInt(valor);
        } else if (contValore === 4) {
            console.log(`Parentesis: ${valor}`);
            parVal = parseInt(valor);
        } else if (contValore === 5) {
            console.log(`Valor: ${valor}`);
            valorVal = valor;
        } else if (contValore === 6) {
            console.log(`Id Campo: ${valor}`);
            idCampo = parseInt(valor);
        } else if (contValore === 7) {
            console.log(`Posision: ${valor}`);
            posVal = parseInt(valor);
        } else if (contValore === 8) {
            console.log(`Longitud: ${valor}`);
            longVal = parseInt(valor);
            contValore = 0;

            addCondicion = true;
        }

        if (addCondicion === true) {
            // alert("Entre Ajax");
            addCondicion = false;
            indexConSave++;
            console.log(`Esta es la condición: ${indexConSave}`);
            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                },
                url: "/tx_filter_details",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "_method": "create",
                    conditionnumber: indexConSave,
                    connector: conectorVal,
                    denied: negadoVal,
                    operator: opeVal,
                    parenthesis: parVal,
                    value: valorVal,
                    fieldFormat_id: idCampo,
                    position: posVal,
                    lenght: longVal,
                    key_id: keyId
                }),

                success: function () {

                    swal({
                        title: "Good job!",
                        text: "Condition added!",
                        type: "success"
                    });
                }
            });
        }

    });
}

//###############################################################################################################################################################################
//###############################################################################################################################################################################
//###############################################################################################################################################################################
// Scripts para Metodo Edit


$(document).ready(function(){
    let valFiltro = $('#tipoFiltro').val();
    $('#typeFilter').val(`${valFiltro}`);

    //-- Id de la sesión a la cual se le agregara el nuevo filtro
    // idSession = $('#filterSession').val();
    // console.log(idSession);
    //
    // for (let i = 0; i < gon.sesiones.length; i++){
    //     let sesionId = gon.sesiones[i].Id
    //     let layId = gon.sesiones[i].Lay_id
    //     let formatId = gon.sesiones[i].Format_id
    //
    //     if ( idSession == sesionId ){
    //         // alert('Sessiones iguales');
    //
    //
    //         for ( let c = 0; c < gon.tCamposFormatos.length; c++ ){
    //
    //             if ( (gon.tCamposFormatos[c].IdLay == layId ) && ( gon.tCamposFormatos[c].IdFormato == formatId) ){
    //
    //                 for( let d = 0; d < gon.tCamposLays.length; d++ ){
    //
    //                     if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){
    //
    //                         if ( gon.tCamposLays[d].Alias != ""){
    //
    //                             $("#campoEdit").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);
    //
    //                         } else{
    //                             $("#campoEdit").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`);
    //                         }
    //
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }
});

//Scripts para detalle del filtro
let valUno,
    valDos,
    parVal,
    negadoVal,
    conectorVal,
    idCampo,
    operadorVal
    ///Traduciones
    and = $("#andPalabra").val(),
    or = $("#orPalabra").val();

//Envia el id de la condición
$('.editCondi').on('click', function(){
    let idCondicion = this.value;
    $('#idCondicion').val(`${idCondicion}`);

    //Limpio valores de selects e inputs
    $('#campoEdit').val('');
    $('#operadorEdit').val('');
    $('#unValor').val('');
    $('#dosValor').val('');
    $('#parEdit').val('');
    $('#posEdit').val('');
    $('#lengEdit').val('');
    $('#conector').remove();

    let btnEdit =  $(this).parent('td');
    parVal = btnEdit.siblings('td.parentesisVal').html(); //Valor parentesis
    console.log(parVal);
    posiVal = btnEdit.siblings('td.positionVal').html(); // Posision campo
    console.log(posiVal);
    lengVal = btnEdit.siblings('td.lengthVal').html(); // Longitud del campo
    console.log(lengVal);
    negadoVal = btnEdit.siblings('td.denegadoVal').html();// valor negado
    console.log(negadoVal);
    conectorVal = btnEdit.siblings('td.conectorVal').html(); // valor conector
    console.log(conectorVal);
    idCampo = btnEdit.siblings('td.expresion').children('span.campo').attr('value'); // valor id campo
    console.log(idCampo);
    operadorVal = btnEdit.siblings('td.expresion').children('strong.operador').attr('value'); // Valor operador
    console.log(operadorVal);
    valUno = $.trim(btnEdit.siblings('td.expresion').children('span.valorUno').attr('value')); // Valor primer Input el otro input se genera mas abajo
    console.log(valUno)

    $('#parEdit').val(`${parVal}`);
    $('#posEdit').val(`${posiVal}`);
    $('#lengEdit').val(`${lengVal}`);

    // Es negado o no?
    if ( negadoVal == "1" ){
        $('#negadoEdit').remove();
        $('.negadoEdit').append(`<input type="checkbox" id="negadoEdit" value=${negadoVal} checked >`);
    } else {
        $('#negadoEdit').remove();
        $('.negadoEdit').append(`<input type="checkbox" id="negadoEdit" value=${negadoVal}>`);
    }

    $('#conectorVal').val(`${conectorVal}`);
    if ( conectorVal == "0" ){
        $('.connectorEdit').append(`<span class="btn btn-primary btn-xs" id='conector' value='${conectorVal}' title="Cambiar Conector">${or}</span>`);
    } else if ( conectorVal == "1" ) {
        $('.connectorEdit').append(`<span class="btn btn-primary btn-xs" id='conector' value='${conectorVal}' title="Cambiar Conector">${and}</span>`);
    }

    // La condicion es una expresión o un parentesis?
    if ( parVal === "0" ){
        //Si esntra aquí es una expresión
        let input2 = $('#dosValor')
        $('#campoEdit').val(`${idCampo}`);
        $('#operadorEdit').val(`${operadorVal}`);
        $('#unValor').val(`${valUno}`);
        input2.css('display','none');

        if ( operadorVal === "8" ){
            input2.css('display','block');
            valDos = btnEdit.siblings('td.expresion').children('span.valorDos').attr('value');
            input2.val(`${valDos}`);
        }

        $('.campoEdit, .operadorEdit, .valuesEdit').css('display','block')


    } else {
        // alert('Soy un Parentesis')
        $('.campoEdit, .operadorEdit, .valuesEdit').css('display','none');
    }


});

$(document).on('click', '#negadoEdit', function(){
    let check = $('#negadoEdit').val();

    if( check == "0" ){
        $('#negadoEdit').val('1');
    } else {
        $('#negadoEdit').val('0');
    };
});

$(document).on('click', '#conector', function(){
    let conector = $('#conectorVal').val();

    if( conector == "0" ){
        $('#conector').html(`${and}`);
        $('#conectorVal').val('1');
    } else {
        $('#conector').html(`${or}`);
        $('#conectorVal').val('0');
    };
});

$('#campoEdit').on('change', function(){
   let idCampo = this.value;

   for (let i = 0; i < gon.tCamposLays.length; i++){
       let campo = gon.tCamposLays[i].IdCampo

       if ( idCampo == campo ){
           let pos =  gon.tCamposLays[i].Posicion,
               long =  gon.tCamposLays[i].Longitud;

           $('#posEdit').val(`${pos}`);
           $('#lengEdit').val(`${long}`);
       }
   }

});

$('#operadorEdit').on('change', function(){
   let val = this.value;

    if ( val == "8" ){
        $('.valorDos').css('display','block');
    } else {
        $('.valorDos').css('display','none');
        $('.valorDos').val('');
    }

});

$('#saveCondi').on('click', function(){
    alert('Entre guardar');

    let id = $('#idCondicion').val();

    //Id del filtro
    let idFiltro = $('#idFilter').val();

    //Detalle del filtro
    let negado =  $('#negadoEdit').val(),
        conector = $('#conectorVal').val(),
        par = $('#parEdit').val(),
        campo = $('#campoEdit').val(),
        posi = $('#posEdit').val(),
        leng = $('#lengEdit').val(),
        operador = $('#operadorEdit').val(),
        values = $('#unValor').val();

    if ( operador == '8'){
        valDos = $('#dosValor').val();
        values = '{'+values+'}{'+valDos+'}';
    }

    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: `/tx_filter_details/${id}`,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "_method": "put",
            connector: conector,
            denied: negado,
            operator: operador,
            paranthesis: par,
            value: values,
            fieldFormat_id: campo,
            position: posi,
            lenght: leng
        }),
        success: function (response) {

            swal({
                title: "Good job!",
                text: "Condition added!",
                type: "success"
            });
        }
    });
});

