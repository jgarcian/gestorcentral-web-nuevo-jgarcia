//= require toastr/toastr.min.js
//= require nestable/jquery.nestable.js
//= require codemirror/codemirror.js
//= require codemirror/mode/javascript/javascript.js
//= require validate/jquery.validate.min.js
//= require jsTree/jstree.min.js
//= require diff_match_patch/javascript/diff_match_patch.js
//= require preetyTextDiff/jquery.pretty-text-diff.min.js
//= require tinycon/tinycon.min.js
//= require idle-timer/idle-timer.min.js
//= require jquery-ui/jquery-ui.min.js
//= require sweetalert/sweetalert.min.js

console.log('EntreATXSessions');

//---Traducciones Inputs
let placeAddList = $('#traduPlaceAddList').val();
    sorrySwal = $('#swalSorry').val(),
    traduCondiIncompleta = $('#swalCondiIncompleta').val(),
    traduCondiExist = $('#swalCondiExiste').val(),
    traduCondiDelete = $('#swalCondiDelete').val(),
    titleCanel = $('#titleCancel').val(),
    txtCancel = $('#txtCancel').val(),
    txtAnd = $('#txtAnd').val(),
    txtOr = $('#txtOr').val(),
    swalDelete = $('#swalDelete').val(),
    dataWillLose = $('#dataWillLose').val(),
    yourSure = $('#youSure').val(),
    btn_yes = $('#btn_yes').val(),
    btn_confir = $('#btn_confir').val(),
    btn_cancel = $('#btn_cancel').val(),
    btn_cancel2 = $('#btn_cancel2').val(),
    noPalabra = $('#noPalabra').val(),
    noAddCone = $('#noAddCone').val(),
    noAddSinCon = $('#noAddSinCon').val(),
    msnBtnDelete = $('#deleteSession').val(),
    session_active_title = $('#session_active_title').val(),
    session_active_txt = $('#session_active_txt').val(),
    name_filter_exists = $('#name_filter_exists').val(),
    name_session_exists = $('#name_session_exists').val(),
    session_inactive = $('#session_inactive').val();


//Time Picker
$('.clockpicker').clockpicker();

//---Estilos para el wizard
$('.steps ul').css('display','flex');
// $('.content').css('min-height','auto');
// $('.content').css('height','auto');

function eliminarSession(id){
    // alert(`Entre Eliminar Session ${id}`);

    $(`tr#session${id}`).fadeOut(300, function(){
        $(this).remove();
        swal( `${msnBtnDelete}` , "success");
    });

}

let arrIdRef = [];
    // numCmpo = 0;
//--- Edita, elimina o agregar campo referencía
// $(document).on('click', '.check-id-Campo', function(){
//
//     let $_this = this,
//         inputArrRef = document.getElementById('arrCmpRe');
//     check = $(this).prop('checked');
//     // val = this.value,
//
//     if( !check ){
//         // alert('eliminar check');
//         let id = $_this.id;
//
//         if( id != ""){
//             // alert(`Tengo un id ${id}`);
//
//             //Eliminar los atributos del checkbox
//             $_this.removeAttribute('id');
//             $_this.removeAttribute('name');
//             $_this.previousElementSibling.removeAttribute('value');
//             $_this.previousElementSibling.removeAttribute('name');
//
//             arrIdRef.push(id)
//         } else {
//             $_this.removeAttribute('name');
//         }
//
//     } else {
//         // alert('Agregar check');
//         numCmpo++;
//         // console.log(arrIdRef.length);
//
//         let longArr = arrIdRef.length;
//
//         if( longArr > 0 ){
//             // alert(`Hay ${longArr} elementos en el arraglo  y son: ${arrIdRef}`);
//
//             $_this.setAttribute('id', `${arrIdRef[0]}`);
//             $_this.setAttribute('name',`tx_shcedule_reference[campo${numCmpo}][field_formatId]`);
//             $_this.previousElementSibling.setAttribute('value',`${arrIdRef[0]}`);
//             $_this.previousElementSibling.setAttribute('name', `tx_shcedule_reference[campo${numCmpo}][id]`);
//
//             arrIdRef.shift();
//         } else {
//             $_this.setAttribute('name',`tx_shcedule_reference[campo${numCmpo}][field_formatId]`);
//         }
//     }
//
//     inputArrRef.setAttribute('value',`${arrIdRef}`);
// });

//-- Guardar campos editados
// $('#updateCpRef').on('click', function(){
//     let id = this.value;
//
//     $.ajax({
//         type: "PUT",
//         beforeSend: function (xhr) {
//             xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
//         },
//         url: `/tx_sessions/${id}.json`,
//         contentType: "application/json",
//         dataType: "json",
//         data: JSON.stringify({
//             "_method": "update",
//             //--- Tbitacora
//             IdUsuario: idUser,
//             msg: msgtxt
//         }),
//         success: function(result, status, xhr){
//
//             console.log('entre load ........' + result + status + xhr);
//
//             swal({
//                 position: 'top-end',
//                 type: 'success',
//                 title: 'Ok',
//                 text: 'Todo bien',
//                 showConfirmButton: true,
//                 timer: 5000
//             });
//         },
//         error: function (xhr, ajaxOptions, thrownError) {
//
//             swal({
//                 position: 'top-end',
//                 type: 'error',
//                 title: '¡Error!',
//                 text: `Error ${xhr.status} en ${thrownError}`,
//                 showConfirmButton: true,
//                 timer: 3000
//             });
//         }
//     });
// });

//--- Eliminar sesiones
$('.eliminarSesion').on('click', function(){
    // alert("Entree Eliminar");

    let id =  this.id;

    let titleDelete = $('#titleDelete').val();
    let titleCancel = $('#titleCancel').val();
    let txtAreSure = $('#txtAreSure').val();
    let txtCancel = $('#txtCancel').val();


    //Texto botnos
    let btnConfirm = $('#btnConfirm').val();
    let btnCancel = $('#btnCancel').val();

    swal({
        title: `${titleDelete}`,
        text: `${txtAreSure}`,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: `${btnConfirm}`,
        cancelButtonText: `${btnCancel}`,
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                },
                url: "/tx_sessions/" + id,
                data: {"_method": "delete"},
            });

            eliminarSession(id);
        }
        else {
            swal(`${titleCancel}`, `${txtCancel}`, "error");
        }
    });
});

//Activar sesión
$('.active').on('click', function(){

    let id = this.id,
        valorActive = this.value;

    if( valorActive == '1' ){
        $(this).val('0');
        valorActive = 0
    } else {
        $(this).val('1');
        valorActive = 1
    }

    // alert(`Id de esta sesión: ${id}, y el valor de ativo es: ${valorActive}`);

    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: "/tx_sessions",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "_method": "update",
            Inactive: valorActive,
            Id: id
        }),
        success: function () {

            if( valorActive == 1 ){
                swal(`${session_active_title}`);
            } else {
                swal(`${session_inactive}`);
            }
        }
    });

});

//funcion que envias ajax estado de el filtro
function update_state_filtro(id, value){

    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: "../tx_filters/estado_filtro/",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            id_filtro: id,
            filtro_estado: value,
        }),
        success: function (data) {

            alert('todos bien')

        }
    });

};

//Estado del filtro Index
function toggle(democ) {

    let id = democ.id;

    console.log(id)

    if (democ.readOnly){
        // aquí esta des checkeado

        alert('Me movi aqui')
        democ.checked=democ.readOnly=false

        let value = 0;

        update_state_filtro(id, value)
    }
    else if (!democ.checked){
        // aquí esta indeterminado
        alert('Ahora aquí')
        democ.readOnly=democ.indeterminate=true;
        let value = 2;

        update_state_filtro(id, value)
    } else {

        //aquí esta chekeado
        alert('Estoy aqui')

        let value = 1;
        update_state_filtro(id, value)
    }

};







//Agregar o eliminar campos de refrencia a la sesión
$(document).on('click', '.check-id-Campo', function(){

    let _this = this,
        id = _this.id,
        value = _this.value;

    if ( id != "" ) {
        // alert(' eliminar campo')

        $.ajax({
            type: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            url: "../tx_schedule_references/agregar_eliminar_cmpref",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                eliminar_campo: true,
                id_registro: id
            }),
            success: function (data) {
                console.log({data})
                _this.removeAttribute('id');
            }
        });

    } else {
        // alert(' agregar campo')

        $.ajax({
            type: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            url: "../tx_schedule_references/agregar_eliminar_cmpref",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                crear_campo: true,
                id_campo: value
            }),
            success: function (data) {

                console.log({data})

                _this.setAttribute('id',`${data['id']}`);

                // swal({
                //     title: "Good job!",
                //     text: "Condition added!",
                //     type: "success"
                // });

            }
        });
    }


});

//############################## SCRIPTS PARA EL METODO NEW ############//

//----Gon Para llenar Select Formatos
$('.selectLay').on('change', function(){
    $(".selectFormat option").remove();
    $(".selectFormat").attr('disabled', false);
    let traduSelectFormat = $('#traduSeleFormat').val();
    id_lay = $(this).val();


    $(".selectFormat").append($("<option value></option>").text(`${traduSelectFormat}`));
    for( f = 0; f < gon.formatos.length; f++ ){
        if( gon.formatos[f].IdLay == id_lay ){

            $(".selectFormat").append(`<option value="${gon.formatos[f].IdFormato}" name="optionFormat">${gon.formatos[f].Descripcion}</option>`)
        } else {
            $(".selectFormat option").remove();
            $(".selectFormat").attr('disabled', true);
            $(".selectFormat").append($("<option value></option>").text(`${traduSelectFormat}`));
        }
    }
});

//----Gon Para Llenar SelectFeild y Select Group
$('.selectFormat').on('change', function() {

    let traSelFeild = $("#traduSeleFeild").val();
    $(".selectFeild option").remove();
    $("#group_sel option").remove();
    $('#campos_alerta, #campos_modal_table').children().remove();
    id_format = $(this).val();
    // console.log(`valor de Layout: ${id_lay} y de Formato: ${id_format}`)

    $(".selectFeild").append($("<option value></option>").text(`${traSelFeild}`));
    for ( c = 0; c < gon.tCamposFormatos.length; c++ ){

        if( ( gon.tCamposFormatos[c].IdLay == id_lay ) && ( gon.tCamposFormatos[c].IdFormato == id_format ) ){

            for( d = 0; d < gon.tCamposLays.length; d++ ){

                if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){

                    //Obtengo el id de campo lo convierto a string y saco su longitud.
                    let idCampo = `${gon.tCamposLays[d].IdCampo}`,
                        idCampoStr = String(idCampo).length,
                        numIdCampo;

                    //Agrega 0, 00 al id dependiendo del decimal del id de campo
                    if ( idCampoStr == '1' ){

                        // console.log(`Agrego 2 ceros a: 00${gon.tCamposLays[d].IdCampo}`);
                        numIdCampo = `00${gon.tCamposLays[d].IdCampo}`;

                    } else if ( idCampoStr == '2' ){

                        // console.log(`Agrego 1 ceros a: 0${gon.tCamposLays[d].IdCampo}`);
                        numIdCampo = `0${gon.tCamposLays[d].IdCampo}`;

                    } else {
                        // console.log(`El Id es de 3 digitos: ${gon.tCamposLays[d].IdCampo}`);
                        numIdCampo = `${gon.tCamposLays[d].IdCampo}`;
                    }



                    if ( gon.tCamposLays[d].Alias != ""){

                        descripcionCampo = gon.tCamposLays[d].Alias

                    } else{
                        descripcionCampo = gon.tCamposLays[d].Nombre

                    }

                    $(".selectFeild").append(`<option value="${idCampo}" name="select_feild">${descripcionCampo}</option>`);
                    $("#group_sel").append(`<option value="${idCampo}" name="select_feild">${descripcionCampo}</option>`);

                    //Se agregan los campos a la table del wizard que esta en el 4 paso
                    $('#campos_alerta').append(`<tr>
                                                    <td>
                                                        <input type="checkbox" value="${idCampo}" class="check-id-Campo">
                                                    </td>
                                                    <td>${descripcionCampo}</td>
                                               </tr>`
                    );

                    $('#campos_modal_table').append(`<tr id="${gon.tCamposLays[d].Longitud}" class="campo_modal_tr">
                                                       <td>${numIdCampo}_${descripcionCampo}</td> 
                                                    </tr>`
                    );

                }
            }

        }
    }

    //Agregando Campos Token
    for ( let it = 0; it < gon.tCamposToken.length; it++){

        if ( (gon.tCamposToken[it].IdLay == id_lay) && (gon.tCamposToken[it].IdFormato == id_format)){

            for ( let t = 0; t < gon.tDefToken.length; t++ ){

                if ( (gon.tCamposToken[it].IdToken == gon.tDefToken[t].IdToken) && (gon.tCamposToken[it].IdCampo == gon.tDefToken[t].IdCampo) ){

                    let valor = `${gon.tDefToken[t].IdToken},${gon.tDefToken[t].IdCampo}`;

                    if(gon.tDefToken[t].Alias != ""){
                        token = `<option value="${valor}" name="select_feild" class="dinamicField" >${gon.tDefToken[t].Alias}</option>`;
                    }else{
                        token = `<option value="${valor}" name="select_feild" class="dinamicField" >${gon.tDefToken[t].Nombre}</option>`;
                    }

                    $(".selectFeild, #group_sel").append(token);
                }
            }

        }
    }

    //----inicilizo chose-select
    $(".chosen-select").chosen();
    $(".chosen-container").css({'width':'350px'});
    $(".default").css({'width':'217px'});
});


//---- Verificar si exista ya el nombre de una sesión
$('#des_session').focusout(function(){

    let nameNewSession = this.value;
    // alert(`${nameNewSession}`);

    for(let i =0; i < gon.sesiones.length; i++){
        // alert('Siiiii')
        let sesionEnBase = gon.sesiones[i].Description;

        if( nameNewSession == sesionEnBase ){
            swal(`${name_session_exists}`);
            $(this).val('');
            break
        }
    }

});

//----Ayuda a mostrar el select-chose en el wizard
setTimeout(function(){
    $('#group_sel_chosen').css('width','100%');
    var hijo = $('.search-field').children();
    hijo.css('width', '100%')
}, 2000);

function selectShow(optionSelected){

    let colorPicker = document.getElementById('colorPicker'),
        choseSelect = document.getElementById('choseSelect'),
        inputColor = document.getElementById('backgroundColor'),
        content = $('.content'),
        cuadroColor = $('.minicolors-swatch-color'),
        selectGroup = $('.chosen-select');


    if ( optionSelected == "principal" ){

        colorPicker.style.display = 'none';
        choseSelect.style.display = 'none';

        //Limpar valores de los demas inputs
        inputColor.value = "";
        inputColor.removeAttribute('required');
        cuadroColor.css({'background-color': ''});

        content.css('min-height','320px');
        selectGroup.val([]).trigger('chosen:updated');


    } else if ( optionSelected == "general" ) {

        colorPicker.style.display = 'block';
        choseSelect.style.display = 'none';
        content.css('min-height','320px');
        inputColor.setAttribute('required', true);
        selectGroup.val([]).trigger('chosen:updated');
        // $('.chosen-choices').html();

    } else if ( optionSelected == "grupo" ) {

        colorPicker.style.display = 'none';
        inputColor.value = "";
        inputColor.removeAttribute('required');
        cuadroColor.css({'background-color': ''});
        choseSelect.style.display = 'block';
        content.css('min-height','420px');

    }
}



// $('.filterSession').on('change', function(){
//     var typeFilter = $(this).val();
//     // alert("Entre changue");
//     // console.log(option)
//
//
// });


//---Crea la paleta de colores con jquery-minicolors-rails
$('.demo').each(function () {
    $(this).minicolors({
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        format: $(this).attr('data-format') || 'hex',
        keywords: $(this).attr('data-keywords') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'bottom left',
        swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
        change: function (value, opacity) {
            if (!value) return;
            if (opacity) value += ', ' + opacity;
            if (typeof console === 'object') {
                //console.log(value);
            }
        },
        theme: 'bootstrap'
    });
});

//---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
$(document).on('change', '.operator_sel', function(){

    $(".betweenValue label").remove();
    $(".inNot label").remove();
    let operadorText = $('.operator_sel option:selected').text();
    let operadorVal = $('.operator_sel').val();

    //---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
    if ( operadorVal === "8" ){
        $('.betweenValue').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .inValue, .contDenied').css('display','none');
        $('.betweenValue').css('display','block');
        $('#valor, #selectList').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else if( operadorVal === "7" ){
        $('.inNot').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .betweenValue, .contDenied').css('display','none');
        $('.inValue').css('display','flex');
        $('#valor, .valOneBetween, .valTwoBetween').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else {

        $('.contDenied, .oneValue').css('display', 'block');
        $('.betweenValue, .inValue').css('display','none');
        $('.valOneBetween, .valTwoBetween, #selectList').val("");

    }

});

//---Script para el boton de carga archivos falso
var btnReal = document.getElementById('realBtnFile');

$('.falseBtnFile').on('click', function(){
    btnReal.click();
});

$('.realBtnFile').change(function(){
    var valFile = $(this).val()

    if ( valFile ){
        let txtFile = valFile.match( /[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
        $('.customText').html(txtFile);
        $('.customText').attr('value', txtFile);
        $('#selectList').fadeOut(100);
    } else {
        $('.customText').html(`${placeAddList}`);
        $('.customText').attr('value', '');
        $('#selectList').fadeIn(100);
    }
});

// Bloqueo select In
$('#selectList').on('change', function(){
    // alert('Entre list')
    let $_this = $(this).val();

    if( $_this !== "" ){
        $('.upFiles').fadeOut(100);
    } else {
        $('.upFiles').fadeIn(100);
    }

});


//---Radio Button i-checks
$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
});

//---Cambia el valor del input Denied
$('.icheckbox_square-green').on('ifClicked', function(){
    let _this = $('.icheckbox_square-green').hasClass('checked');

    if ( !_this ){

        $('#denied').val('1');

    } else {

        $('#denied').val('0');

    }
});

// var containers = $('#drag-elements').toArray();
// containers.concat($('.drop-target').toArray());

// let condiciones = $('#drag-elements');
//
// //---Drag & Drop Dragula
// let drake = dragula([condiciones[0]],{
//
//     // isContainer: function (el){
//     //     return el.classList.contains('drop-target');
//     // },
//     //
//     // copy: function (el, source){
//     //     return source === document.querySelector('#drag-elements')
//     // },
//     //
//     // accepts: function (el, target){
//     //     return target !== document.querySelector('#drag-elements')
//     // }
//
// });

let left = $('#drag-elements');

var drake =  dragula([left[0]],{
    revertOnSpill: true
});

function ordenFiltro(){
    let allCondi = $('#drag-elements').children(); // Todas las condiciones
    let btnAnd = `<span class="btn btn-primary btn-xs changeConector">${txtAnd}</span>`;

    $('#drag-elements tr.condicion').each(function(){
        let allIndex_Condiciones = $(this).index();  // Index de todas las tr con clase "condicion"
        // alert(allIndex_Condiciones);
        let indexConArriba = allIndex_Condiciones - 1;
        let queHayArriba = allCondi.eq(indexConArriba); // Selecionando en elementos que esta de bajo de parAbre
        // console.log(queHayArriba);

        if ( allIndex_Condiciones !== 0){

            if( queHayArriba.hasClass('condicion') || queHayArriba.hasClass('parCierre') ){

                if( $(this).children('td.conectorVal').html() === "-1"  ){
                    $(this).children('td.connector').html(btnAnd);
                    $(this).children('td.conectorVal').html('1');
                }

            } else if( queHayArriba.hasClass('parAbre') ){

                if( $(this).hasClass('condicion') ){
                    $(this).children('td.connector').html('');
                    $(this).children('td.conectorVal').html('-1');
                }
            }

        } else {
            $(this).children('td.connector').html('');
            $(this).children('td.conectorVal').html('-1');
        }

    });

    $('#drag-elements tr.parAbre').each(function(){
        let allIndex_Parentesis = $(this).index();
        let indexConArriba = allIndex_Parentesis- 1;
        let queHayArriba = allCondi.eq(indexConArriba);

        if( allIndex_Parentesis === 0 || queHayArriba.hasClass('parAbre')){

            $(this).children('td.connector').html('');
            $(this).children('td.conectorVal').html('-1');

        } else {

            if($(this).children('td.conectorVal').html() === '-1'){
                $(this).children('td.connector').html(btnAnd);
                $(this).children('td.conectorVal').html('1');
            }

        }

    });
};

// Handle Events
drake.on('drop', function(el, target, source, sibling){
    ordenFiltro();
});


//######## AGREGAR CONDICIONES Y VALIDACIONES PARA NO REPETIR CONDICIONES #######################
let arrayOneValue = [];

let arrayBetween = [];

let arrayList = [];

//---Número de Clase
let numCon = 0;

//---Efecto fade
let fadeIn = 1000;

function condicionIncompleta(){
    swal({
        title: `${sorrySwal}`,
        text: `${traduCondiIncompleta}`,
        type: "error"
    });
}

function condiExiste(){
    swal({
        title: `${sorrySwal}`,
        text: `${traduCondiExist}`,
        type: "error"
    });
}


$(document).on('click', '.addConditions', function(){

    let conditionText;
    let denegadoBetweenList;

    //-- Texto de los Select
    let campoText = $('.selectFeild option:selected').text(),
        operadorText = $('.operator_sel option:selected').text(),
        listText = $('#selectList option:selected').text(),
        listUpload = $('#customText').text(),
        connector =  $('#selectConnector option:selected').text();

    //-- Valores de los Select
    let campoVal = $('.selectFeild').val();
    let operadorVal = $('.operator_sel').val();
    let conectorVal = $('#selectConnector').val();

    //-- Valores de Listas
    let selectListVal = $('#selectList').val();
    let fileUpload = $('#realBtnFile').val();

    //-- Valores de los Inputs
    let unValor = $('#valor').val();
    let valOneBetween = $('.valOneBetween').val();
    let valTwoBetween = $('.valTwoBetween').val();

    //-- Check de Negado
    let denied = $('#denied').val();

    //---Agrega la palabra "NO" - menos Between / Not Between - In / Not In
    if( denied === "1"){

        negado = `${noPalabra}`;
        tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;

    } else if ( denied === "0" ) {

        negado = "";
        tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

    }


    //---GON TABLAS
    let campos = gon.tCamposLays,
        camposToken = gon.tDefToken;



    //-- Guardo la posision y longitud del campo selecionado en una td
    let valCampoSplit = campoVal.split(','),
        td,
        idCampoValor;

    if ( valCampoSplit.length == 1 ){

        for( let i = 0; i < campos.length; i++ ){

            let campo = campos[i];
            idCampoValor = valCampoSplit[0];

            if( campo.IdCampo == valCampoSplit[0] ){

                td = `<td class="tdDispNone filterCondi" value="${campo.Posicion}" name="posision">${campo.Posicion}</td>
                      <td class="tdDispNone filterCondi" value="${campo.Longitud}" name="longitud">${campo.Longitud}</td>
                      <td class="tdDispNone filterCondi" value="0" name="token_id">0</td>`;
            }
        }

    } else if ( valCampoSplit.length == 2 ) {

        for( let i = 0; i < camposToken.length; i++ ){

            let campo = camposToken[i];

            if(  (campo.IdToken == valCampoSplit[0]) && (campo.IdCampo == valCampoSplit[1]) ){

                // alert(`Name: ${campo.Nombre} ${campo.Alias} Son ${campo.IdToken} - ${valCampoSplit[0]} && Son ${campo.IdCampo} - ${valCampoSplit[1]}`);

                idCampoValor = valCampoSplit[1];

                if( campo.LongDespliegue > 0){
                    tokenLong = campo.LongDespliegue
                } else {
                    tokenLong = campo.Longitud
                }

                td = `<td class="tdDispNone filterCondi" value="${campo.Posicion}" name="posision">${campo.Posicion}</td>
                      <td class="tdDispNone filterCondi" value="${tokenLong}" name="longitud">${tokenLong}</td>
                      <td class="tdDispNone filterCondi" value="${valCampoSplit[0]}" name="token_id">${valCampoSplit[0]}</td>`;
            }
        }
    }

    // alert(`Valor de campo ${idCampoValor}`);

    let numDeCondi = $('.tBodyCondition').children().length;
    if( numDeCondi === 0 ){
        conectorVal = -1;
        // connector = "";
        tdConector = `<td class="text-center connector"> </td>`;
    } else {
        tdConector = `<td class="text-center connector"> <span class="btn btn-primary btn-xs changeConector" title="Cambiar conector">${connector}</span></td>`;
    }

    tdEliminar = `<td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>`;

    if ( "" !== unValor) {

        numCon++;

        //---Creo una variable donde concateno todos los valores
        let conditionText = `${campoText} ${operadorText} ${unValor}`;


        if( campoVal !== ""){

            tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}"> 
                      ${tdDenegado}
                      ${tdConector}
                      <td class="text-center expresion"><strong class="negado">${negado}</strong> <span class="campo" value="${idCampoValor}">${campoText}</span> <strong class="operador" value="${operadorVal}">${operadorText}</strong> <span class="valorUno" value="${unValor}">${unValor}</span> </td>
                      ${tdEliminar}
                      <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                      <td class="tdDispNone filterCondi denegadoVal" value="${denied}" name="denegado">${denied}</td>
                      <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                      <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                      <td class="tdDispNone filterCondi" value="${unValor}" name="valor">${unValor}</td>
                      <td class="tdDispNone filterCondi" value="${idCampoValor}" name="campo">${idCampoValor}</td>
                      ${td}
                  </tr>`;
        }

        function agregarCondiOneVal(){

            if( ( unValor !== "" && campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( unValor !== "" && campoVal !== "" && operadorVal !== "" )){

                $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

                //Mueve el scroll hacia abajo donde se agrega la condición
                $('.s_Condition').animate({
                    scrollTop: $(document).height()
                }, 'slow');

                arrayOneValue.push(`${conditionText}`);

                // console.log(arrayOneValue);

                //--Habilita select de los conectores
                $('#selectConnector').prop('disabled', false);


            } else {
                condicionIncompleta();
            }

        }

        if ( arrayOneValue.length !== 0 ){

            let isDuplicateOne = false;

            //---Recorro todos los valores almacenados en el arreglo para checar si ya existe la condición
            for ( let i = 0; i < arrayOneValue.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                let conditionArr = arrayOneValue[i];

                //---Camparo valores obtenidos de los inputs con los valores que ya existen en el arreglo
                if ( conditionText === conditionArr ){
                    numCon--;

                    condiExiste();

                    isDuplicateOne = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateOne ){
                agregarCondiOneVal()
            }

        } else{
            agregarCondiOneVal()
        }

        //---Se limpian valores
        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('#valor').val("");
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

        ///-----Valido que los inputs de Between no esten vacios
    }
    else if ( valOneBetween !== "" && valTwoBetween !== "" ){

        numCon++;

        //---Creo Una variable donde concateno los valores Obtenidos de los inputs Between
        conditionText = `${campoText} Between ${valOneBetween} AND ${valTwoBetween}`;

        if( operadorText === " Between "){

            denegadoBetweenList = 0;
            negadoBetIn = "";
            tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

        } else {

            denegadoBetweenList = 1;
            negadoBetIn = `${noPalabra}`;
            tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;
        }

        //---Varibales para construir la Fila
        // let parAbre = `<strong class="parAbre" value="1">${parentesis}</strong>`;
        // let parCierre = `<strong class="parCierra" value="2">)</strong>`;
        let negadoCampo = `<strong class="negado">${negadoBetIn}</strong>`;
        let campo = `<span class="campo" value="${campoVal}">${campoText}</span>`;
        let operador = `<strong class="operador">Between</strong>`;
        // let menorQue = '<strong class="operador"><=</strong>';
        let valorUno = `<span class="valorUno" value="${valOneBetween}">${valOneBetween}</span>`;
        let valorDos = `<span class="valorDos" value="${valTwoBetween}">${valTwoBetween}</span>`;
        // let conectorAnd = '<strong class="conector">AND</strong>';

        if( campoVal !== ""){
            tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}">
                     ${tdDenegado}
                     ${tdConector}
                     <td class="text-center expresion">${negadoCampo} ${campo} ${operador} ${valorUno} <strong class="conector">${txtAnd}</strong> ${valorDos}</td>
                     ${tdEliminar}
                     <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                     <td class="tdDispNone filterCondi denegadoVal" value="${denegadoBetweenList}" name="denegado">${denegadoBetweenList}</td>
                     <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                     <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                     <td class="tdDispNone filterCondi" value="{${valOneBetween}}{${valTwoBetween}}" name="valor">{${valOneBetween}}{${valTwoBetween}}</td>
                     <td class="tdDispNone filterCondi" value="${idCampoValor}" name="campo">${idCampoValor}</td>
                     ${td}
                  </tr>
                  `;
        }


        function agregarCondiBetween(){

            if( (campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( campoVal !== "" && operadorVal !== "" )){
                //---Imprime Fila
                $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

                //--Mover scroll abajo
                $('.s_Condition').animate({
                    scrollTop: $(document).height()
                }, 'slow');

                //--Habilita select de los conectores
                $('#selectConnector').prop('disabled', false);

                arrayBetween.push(`${conditionText}`);

                // console.log(arrayBetween);

            } else {
                condicionIncompleta();
            }

        }
        //---Si hay elementos en el arreglo se ejecuta las sig. lineas
        if ( arrayBetween !== 0){

            let isDuplicateTwo = false;

            //---Recorro todos los valores almacenados en el arreglo Between
            for ( let i = 0; i < arrayBetween.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                let conditionArr = arrayBetween[i];
                console.log(conditionArr);

                //---Si los valores obetenidos de Feild, Operatos e Inputs Between ya existen en el Arreglo se ejecuta las sig. lineas
                if ( conditionText == conditionArr ){
                    numCon--;

                    condiExiste();

                    isDuplicateTwo = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateTwo ){
                agregarCondiBetween()
            }
        }
        else{
            agregarCondiBetween()
        }



        //---Se limpian los valores
        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('.valOneBetween').val("");
        $('.valTwoBetween').val("");
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');
    }
    else if ( selectListVal !== "" || fileUpload !== "" ) {

        numCon++;

        if ( selectListVal !== "" ){

            var lista = `${listText}`,
                listVal = $('#selectList').val();

            //---Creo una variable donde concateno todos los valores
            conditionText = `${campoText} In ${listText}`;

        } else {

            var lista = `${listUpload}`,
                listVal = `${listUpload}`;

            //---Creo una variable donde concateno todos los valores
            conditionText = `${campoText} In ${listUpload}`;
        }

        if ( operadorText === " In "){

            denegadoBetweenList = 0;
            negadoBetIn = "";
            tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

        } else {

            denegadoBetweenList = 1;
            negadoBetIn = "No";
            tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;

        }

        if( campoVal !== ""){
            tr = `<tr id="condicion${numCon}" class="condicion" value="${conditionText}" name="${operadorText}">
                      ${tdDenegado}
                      ${tdConector}
                      <td class="text-center expresion"><strong class="negado">${negadoBetIn}</strong> <span class="campo" value="${campoVal}">${campoText}</span> <strong class="operador" value="${operadorVal}">In</strong> <span class="lista" value="${listVal}"> ${lista} </span> </td>
                      ${tdEliminar}
                      <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                      <td class="tdDispNone filterCondi denegadoVal" value="${denegadoBetweenList}" name="denegado">${denegadoBetweenList}</td>
                      <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                      <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                      <td class="tdDispNone filterCondi" value="${lista}" name="valor">${lista}</td>
                      <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
                      ${td}
                    </tr>`;
        }


        function agregarCondiList(){

            if( ( campoVal !== "" && operadorVal !== "" && conectorVal !== "" ) && ( campoVal !== "" && operadorVal !== "" ) ){

                let appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

                $('.s_Condition').scrollTop( appCondition.offset().top );

                //--Habilita select de los conectores
                $('#selectConnector').prop('disabled', false);

                arrayList.push(`${conditionText}`);

                console.log(arrayList);

            } else {
                condicionIncompleta();
            }
        }

        if ( arrayList.length !== 0 ){

            var isDuplicateOne = false;

            //---Recorro todos los valores almacenados en el arreglo para checar si ya existe la condición
            for ( let i = 0; i < arrayList.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                var conditionArr = arrayList[i];
                console.log(conditionArr);

                //---Camparo valores obtenidos de los inputs con los valores que ya existen en el arreglo
                if ( conditionText === conditionArr ){
                    numCon--;

                    condiExiste();

                    isDuplicateOne = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateOne ){
                agregarCondiList()
            }

        } else{
            agregarCondiList()
        }

        $('.upFiles').fadeIn(100);
        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('#selectList').val("").fadeIn(100);
        $('#realBtnFile').val("");
        $('.customText').html(`${placeAddList}`);
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else {
        condicionIncompleta()
    }

    $('.s_Condition').css({
        'background-color': '#fff'
    });

});

//-- Eliminar Condiciones del arreglo
function eliminarCondicion(elemento, arrayConditions ){
    // alert("Entre Funcion borrar arreglo");
    // console.log(elemento);
    // console.log(arrayBetween);

    for ( let k in arrayConditions ){
        let condicion = arrayConditions[k];

        if ( elemento === condicion){
            // alert(` Estos ${elemento} es igual a esto ${condicion}`);

            let indice = arrayConditions.indexOf(condicion);

            arrayConditions.splice(indice, 1);

            console.log(arrayConditions);

            swal({
                title: "OK!",
                text: `${traduCondiDelete}`,
                type: "success"
            });

            let numCondi = $('.tBodyCondition').children().length;

            if( numCondi === 0 ){
                $('#selectConnector').prop('disabled', true);
            }

        }
    }

}

let colors = ['rgb(246, 229, 141,.5)', 'rgb(255, 190, 118,.5)', 'rgb(186, 220, 88,.5)', 'rgb(223, 249, 251,.5)', 'rgb(253, 121, 168,.5)', 'rgb(225, 112, 85,.5)'],
    indexColor = 0;

//-- Agregar bloque de Parenthesis
$(document).on('change', '#selectParentesis', function(){
   // alert("Entre Parentesis");

   let longCondi = $('.tBodyCondition').children().length;
   let valorConector = $(this).val();
   let valNegado = $('#selectParentesis option:selected').attr('name')
   let conectorTxt = $('#selectParentesis option:selected').attr('title');
   let parText = $('#selectParentesis option:selected').text();


    //---Agrega la palabra "NO" - menos Between / Not Between - In / Not In
    if( valNegado === "1" ){

        negadoPar = `${noPalabra}`;
        tdDenegadoPar = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;

    } else {

        negadoPar = "";
        tdDenegadoPar = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

    }


    numCon++;

    if( longCondi !== 0 ){
         spanConector = `<span class="btn btn-primary btn-xs changeConector" title="Cambiar conector">${conectorTxt}</span>`;
    } else {
        spanConector = '';
    }

    let abrePartr = `<tr id="condicion${numCon}" class="parAbre" style="background-color: ${colors[indexColor]};">
                  ${tdDenegadoPar}
                  <td class="text-center connector"> ${spanConector}</td>
                  <td class="text-center expresion"><strong class="negado">${negadoPar}</strong> <strong class="parenthesis">(</strong></td>
                  <td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>
                  <td class="tdDispNone filterCondi conectorVal" value="${valorConector}" name="conector">${valorConector}</td>
                  <td class="tdDispNone filterCondi denegadoVal" value="${valNegado}" name="denegado">${valNegado}</td>
                  <td class="tdDispNone filterCondi" value="0" name="operador">-1</td>
                  <td class="tdDispNone filterCondi" value="1" name="parantesis">1</td>
                  <td class="tdDispNone filterCondi" value="0" name="valor"></td>
                  <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
              </tr>`;


    let cierrePartr = `<tr class="condicion${numCon} parCierre" style="background-color: ${colors[indexColor]};">
                              <td class="text-center enegado"></td>
                              <td class="text-center connector"></td>
                              <td class="text-center expresion parCierre"><strong class="parenthesis">)</strong></td>
                              <td class="text-center"></td>
                              <td class="tdDispNone filterCondi conectorVal" value="-1" name="conector">-1</td>
                              <td class="tdDispNone filterCondi denegadoVal" value="0" name="denegado">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="operador">-1</td>
                              <td class="tdDispNone filterCondi" value="2" name="parantesis">2</td>
                              <td class="tdDispNone filterCondi" value="0" name="valor"></td>
                              <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
                          </tr>`;

   if( longCondi !== 0 ) {
       // alert("Entre mayor");

       if( valorConector === "-1" ){
           swal({
               title: "ERROR!",
               text: `${noAddSinCon}`,
               type: "error"
           });

           numCon--;
           indexColor--;

       } else {

           $('.tBodyCondition').append($(abrePartr).fadeIn(fadeIn) );
           let appCondition = $('.tBodyCondition').append($(cierrePartr).fadeIn(fadeIn) );

           $('.s_Condition').scrollTop( appCondition.offset().top );

       }

   } else {

       if( valorConector === "0" || valorConector === "1"){

           swal({
               title: "ERROR!",
               text: `${noAddCone}`,
               type: "error"
           });

           numCon--;
           indexColor--;

       } else {

           $('.tBodyCondition').append($(abrePartr).fadeIn(fadeIn) );
           let appCondition = $('.tBodyCondition').append($(cierrePartr).fadeIn(fadeIn) );

           $('.s_Condition').scrollTop( appCondition.offset().top );

           //--Habilita select de los conectores
           $('#selectConnector').prop('disabled', false);
       }
   }

   $(this).val("");

   indexColor++;
   if( indexColor === 6){

       indexColor = 0;
   }

});

//--Cambiar Negado en la condicion
$(document).on('click', 'input.changeDenied', function(){
    // alert("entre Conector");

    let tdPadre = $(this).parent();
    let tdDenegado = tdPadre.siblings('td.denegadoVal');
    let strongNo = tdPadre.siblings('td.expresion').find('strong.negado');
    // let strongNo = tdExpresion.children();

    if ( tdDenegado.html() === "0" ){
        tdDenegado.html('1');
        strongNo.html('No');
    } else {
        tdDenegado.html('0');
        strongNo.html('');
    }

});

//--Cambiar conector
$(document).on('click', 'span.changeConector', function(){
    // alert("entre Conector");

    let tdPadre = $(this).parent();
    let tdConector = tdPadre.siblings('td.conectorVal');

    if ( tdConector.html() === "0" ){

        tdConector.html('1');
        $(this).html(`${txtAnd}`);

    } else {
        tdConector.html('0');
        $(this).html(`${txtOr}`);
    }

});

// Eliminar elementos del Filtro

// El evento click esta siendo detectado por el contenedor padre, pero este asu vez delega el evento a sus hijos en este caso di.clone
$(document).on('click', 'span.eliminar', function() {
    // alert("Entre eliminar");

    let trPadre = $(this).parents('tr');
    let esPar = trPadre.hasClass('parAbre');
    let conEliminar = trPadre.attr('id');
    console.log(conEliminar);

    let value = trPadre.attr('value');
    let name = trPadre.attr('name');


    swal({
            title: `${swalDelete}`,
            text: `${dataWillLose}`,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: `${btn_yes}`,
            cancelButtonText: `${btn_cancel}`,
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {

            if (isConfirm){

                swal({
                        title: `${swalDelete}`,
                        text: `${yourSure}`,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: `${btn_confir}`,
                        cancelButtonText: `${btn_cancel2}`,
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {

                        if (isConfirm){


                            if( !esPar ){

                                $(`tr#${conEliminar}`).remove();

                                ordenFiltro();

                                if ( name === " Between " || name === " Not Between " ){

                                    eliminarCondicion(value, arrayBetween);

                                } else if ( name === " In " || name === " Not In " ){

                                    eliminarCondicion(value, arrayList);

                                } else {

                                    eliminarCondicion(value, arrayOneValue);

                                }

                            } else {

                                $(`tr#${conEliminar}`).remove();

                                $(`tr.${conEliminar}`).remove();

                                ordenFiltro();

                                swal({
                                    title: "OK!",
                                    text: `${traduCondiDelete}`,
                                    type: "success"
                                });

                                let numCondi = $('.tBodyCondition').children().length;

                                if( numCondi === 0 ){
                                    $('#selectConnector').prop('disabled', true);
                                }
                            }

                        }
                        else{
                            swal(`${titleCanel}`,`${txtCancel}`, "error");
                        }
                    });


            } else {
                swal(`${titleCanel}`,`${txtCancel}`, "error");
            }

        });

});

let condicionesFiltro = [];
let colorLabel = "";


///########################################### Scripts para la configuración del horario Ventana modal

// let longitud;
// $('#campos_modal_table').on('dblclick', 'tr', function(){
//     let $_this = this,
//         txtCampo = $(this).children('td').eq(0).html();
//
//     longitud = $_this.id;
//
//     // alert('Doble click');
//     console.log($_this);
//     console.log(txtCampo);
//
//     //Pimer Campo de Longitd
//     $('#long_campo, #longInputConf').val(`${longitud}`); //campo oculto de la longitud
//     $('#posInputConf').val('1');
//     //Labels con longitud y texto de campo selecionado
//     $('#longCampoSelect').html(`${longitud}`);
//     $('#campoSelect').html(`${txtCampo}`);
// });
//
// function fadeCampos(selector){
//     selector.fadeIn().siblings().fadeOut();
//     // selector.siblings().find('input').val('');
// }
//
// //Muestra los inputs dependiendo del radio elegido y ejecuta la función de arriba
// $('.content-radios').on('click', 'input:radio', function(){
//     let tipo = this.id
//     // alert(`Click en el radio: ${tipo}`);
//
//     if ( tipo == 'primeros' ){
//
//         fadeCampos($('div.primeros'))
//
//     } else if ( tipo == 'ultimos' ) {
//
//         fadeCampos($('div.ultimos'))
//
//     } else {
//         fadeCampos($('div.rangos'))
//     }
//
// });
//
// function agregaCeros( valor , longitudValor ){
//     alert('Entre funcion')
//     console.log(`Soy valor: ${valor}`)
//     console.log(`Soy longitud del campo: ${longitudValor}`)
//
//     let newVal;
//
//     if( longitudValor == "1" ){
//         alert('Agregando 2 ceros')
//         newVal = `00${valor}`;
//
//     } else if (longitudValor == '2'){
//
//         alert('Agregando 1')
//         newVal = `0${valor}`;
//
//     } else if ( longitudValor == '3') {
//         alert('Sin ceros')
//         newVal = `${valor}`;
//     }
//
//     return newVal;
// }
//
// //Agregar comentario al text area
// $('#btnAddMsn').on('click', function(){
//     alert('Entre add')
//     let radioCheck = $('.content-radios').find('input:checked').attr('id'),
//         nameCampo = $('#campoSelect').html(),
//         contentTextA = $('#mensaje_alerta').val(),
//         nomenclature;
//
//     if ( radioCheck == 'primeros' ){
//         alert('Entre Primeros')
//         let primInput = $('#primerosInp').val(),
//             tamaPrim = primInput.length,
//             result = agregaCeros( primInput, tamaPrim );
//
//         nomenclature = `{${nameCampo}-P(${result})}`;
//
//     } else if ( radioCheck == 'ultimos' ) {
//         alert('Entre ultimos')
//
//         let ultimInput = $('#ultimosInp').val(),
//             tamaUltim = ultimInput.length,
//             result = agregaCeros( ultimInput, tamaUltim );
//
//         nomenclature = `{${nameCampo}-U(${result})}`;
//
//     } else {
//
//         let posInput = $('#posInputConf').val(),
//             lengInput = $('#longInputConf').val(),
//             tamaPos = posInput.length,
//             tamaLeng = lengInput.length;
//
//
//         let rUno = agregaCeros( posInput, tamaPos ),
//             rDos = agregaCeros(lengInput, tamaLeng);
//         console.log(rUno)
//         console.log(rDos)
//
//         nomenclature = `{${nameCampo}-D(${rUno},${rDos})}`;
//
//     }
//
//     if( $('#dividir').prop('checked') ){
//         let str1 = nomenclature.split('}');
//
//         nomenclature = `${str1[0]}/100}`;
//     }
//
//     console.log(nomenclature)
//
//     $('#mensaje_alerta').val(`${contentTextA} ${nomenclature}`);
//     $('#longCampoSelect').html(``);
//     $('#campoSelect').html(``);
//     $('.content_config').find('input').val('');
//     $('#dividir').attr('checked', false);
// });
//
// //Evalua el cambios de los checkbox elejido
// $('.inputsCheckboxMsn').on('click', 'input', function(){
//     // alert('Soy el input'+ this.id );
//
//     let valInput = this.value;
//
//     if( valInput == "0"){
//         $(this).val('1')
//     } else {
//         $(this).val('0')
//     }
//
// });
//
// function resetModal(){
//     $('#mensaje_alerta').val('');
//     $('.inputsCheckboxMsn').find('input').attr('checked', false);
//     $('#fechaMsn, #desMsn, #escalaMsn, #limitesMsn, #referenciMsn').val('0');
// }
//
// let contAlert = 0;
// $('#saveMsn').on('click', function(){
//     let mensajeAlerta = $('#mensaje_alerta').val(),
//         fechaMsn = $('#fechaMsn').val(),
//         desMsn = $('#desMsn').val(),
//         escalaMsn = $('#escalaMsn').val(),
//         limitesMsn = $('#limitesMsn').val(),
//         referenciMsn = $('#referenciMsn').val();
//
//     if( mensajeAlerta != ''){
//         contAlert++;
//         let trMsn = `<tr>
//                         <td class="text-center mensajeInfo">${contAlert}</td>
//                         <td class="mensajeInfo">${mensajeAlerta}</td>
//                         <td class="mensajeInfo" style="display: none">${fechaMsn}</td>
//                         <td class="mensajeInfo" style="display: none">${desMsn}</td>
//                         <td class="mensajeInfo" style="display: none">${escalaMsn}</td>
//                         <td class="mensajeInfo" style="display: none">${limitesMsn}</td>
//                         <td class="mensajeInfo" style="display: none">${referenciMsn}</td>
//                     </tr>`;
//
//         $('.mensaje-de-alerta').append(trMsn);
//
//         //resetear valores de la modal
//         resetModal();
//
//         if( contAlert == 2 ){
//
//             $('.optianal').css('display','block');
//             $('#select_intermediate_time').val('0');
//             $('#select_intermediate_time').val('0');
//             $('#select_timeout_time').val('0');
//             $('#intermediate').val('');
//             $('#timeOut').val('');
//
//         }
//     }
// });
//
// $('#btncloseModal').on('click', function(){
//    resetModal();
// });
//
// // Guardar Filtro del metodo new esta función la ejecuta el boton Finis del Wizard
function creacionSesion(){

    // Id Usuario Logeado
    let valId = $('#userId').val();
    keyId = parseInt(valId);
    console.log(keyId);

    let valor = "";
    let td = $('.tBodyCondition').find('td.filterCondi');

    // Valores de la Session
    let idLay = $('#layout').val();
    let idFormato = $('#format').val();
    let des_session = $('#des_session').val();

    // Valores Filtro
    let des_filter = $('#des_filter').val();
    let filterType = $('#typeFilter').val();

    if ( filterType != "0" && filterType != "" ){
        colorLabel = $('.minicolors-input').val();
    }


    //Valores de detalle de filtro
    td.each(function () {
        valor = $(this).html();
        condicionesFiltro.push(valor);
    });

    //Valores de campos de referencias
    let valoresInput = $('#campos_alerta').find('input:checked'),
        arrCamposRef = [];
    console.log(valoresInput);

    valoresInput.each(function(){
        let idCampo = $(this).val();
        console.log($(this).val());

        arrCamposRef.push(idCampo);

    });

    //Valores de horario y mensaje de alerta
    let hr_start = $('#time_start').val(),
        hr_end = $('#time_end').val(),
        tipoLimite = $('#limite').val(),
        limite = $('#limit_tran').val(),
        umInterval = $('#select_Interval_time').val(),
        interval = $('#interval').val(),
        umIntermediate = $('#select_intermediate_time').val(),
        intermediate = $('#intermediate').val(),
        umTimeout = $('#select_timeout_time').val(),
        timeOut = $('#timeOut').val();


    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: "/tx_sessions/",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "_method": "create",
            idLay: idLay,
            idFormato: idFormato,
            nameSession: des_session,

            nameFilter: des_filter,
            typeFilter: filterType,
            general: colorLabel,

            filtro: condicionesFiltro,

            camposRef: arrCamposRef,

            time_start: hr_start,
            time_end: hr_end,
            limiteTipo: tipoLimite,
            limit: limite,
            umInter: umInterval,
            inter: interval,
            umInterm: umIntermediate,
            interme: intermediate,
            umTimeO: umTimeout,
            timeO: timeOut,

            keyId: keyId
        }),
        success: function () {

            swal({
                title: "Good job!",
                text: "Condition added!",
                type: "success"
            });

            window.location.href = '/tx_sessions/';
        }
    });
}


//############################## SCRIPTS PARA CREAR UNICAMENTE FILTRO DESDE EL BOTON NUEVO FILTRO DEL INDEX DE SESSIONS ############//
//Resetea los todos los valores en la modal de mensaje
function resetModal(){
    $('#id_msn_alert').val('');
    $('#mensaje_alerta').val('');
    $('.inputsCheckboxMsn').find('input').attr('checked', false);
    $('label.btn').removeClass('active');
    $('#fechaMsn, #desMsn, #escalaMsn, #limitesMsn, #referenciMsn').val('0');
    $('.modal-dos').fadeOut(300);
}

//Oculta inputs en la modal del mensaje dependiendo del radio button que eligan
function fadeCampos(selector){
    selector.fadeIn().siblings().fadeOut();
}

// funcion que agrega ceros al id del campo
function agregaCeros( valor , longitudValor ){
    // alert('Entre funcion')
    console.log(`Soy valor: ${valor}`)
    console.log(`Soy longitud del campo: ${longitudValor}`)

    let newVal;

    if( longitudValor == "1" ){
        // alert('Agregando 2 ceros')
        newVal = `00${valor}`;

    } else if (longitudValor == '2'){

        // alert('Agregando 1')
        newVal = `0${valor}`;

    } else if ( longitudValor == '3') {
        // alert('Sin ceros')
        newVal = `${valor}`;
    }

    return newVal;
}

let nameFilter;
$(document).on('focusout', '#tx_filters_Description', function(){
    nameFilter = $('#tx_filters_Description').val();

    for(let i = 0; i < gon.desFiltros.length; i++){
        let description = gon.desFiltros[i].Description;

        if( nameFilter === description){
            swal(`${name_filter_exists}`);
            $(this).val('');
            break
        }
    }
});

//---Muestra la segunda modal para generar mensaje y oculta primer modal
$(document).on('click', '#addMsn', function(){
    $('.modal-dialog').fadeOut(300);
    $('.modal-dos').fadeIn(300);
});

//--Oculta segunda modal limpia valores y muestra la primer modal
$(document).on('click', '#cerrar_modal_dos', function(){
    // alert('Entree')
    resetModal();
    $('.modal-dialog').fadeIn(300);
    $('.modal-dos').fadeOut(300);
});

let longitud;
$(document).on('click', 'tr.campo_modal_tr', function(){
    let $_this = this,
        txtCampo = $(this).children('td').eq(0).html();

    longitud = $_this.id;

    // alert('Doble click');
    console.log($_this);
    console.log(txtCampo);

    //Pimer Campo de Longitd
    $('#long_campo, #longInputConf').val(`${longitud}`); //campo oculto de la longitud
    $('#posInputConf').val('1');
    //Labels con longitud y texto de campo selecionado
    $('#longCampoSelect').html(`${longitud}`);
    $('#campoSelect').html(`${txtCampo}`);
});

//Muestra los inputs dependiendo del radio elegido y ejecuta la función de arriba
$(document).on('click', 'input:radio', function(){
    let tipo = this.id;

    if ( tipo == 'primeros' ){

        fadeCampos($('div.primeros'))

    } else if ( tipo == 'ultimos' ) {

        fadeCampos($('div.ultimos'))

    } else {
        fadeCampos($('div.rangos'))
    }

});

//Agregar comentario al text area
$(document).on('click', '#btnAddMsn', function(){
    // alert('Entre add')
    let radioCheck = $('.content-radios').find('input:checked').attr('id'),
        nameCampo = $('#campoSelect').html(),
        contentTextA = $('#mensaje_alerta').val(),
        nomenclature,
        posInVal = $('#posInputConf').val(),
        longInVal = $('#longInputConf').val(),
        primeInVal = $('#primerosInp').val(),
        ultinVal = $('#ultimosInp').val();


    if( ( posInVal != '' && longInVal != '' ) || primeInVal != '' || ultinVal != '' ){
        if ( radioCheck == 'primeros' ){
            // alert('Entre Primeros')
            let primInput = $('#primerosInp').val(),
                tamaPrim = primInput.length,
                result = agregaCeros( primInput, tamaPrim );

            nomenclature = `{${nameCampo}-P(${result})}`;

        } else if ( radioCheck == 'ultimos' ) {
            // alert('Entre ultimos')

            let ultimInput = $('#ultimosInp').val(),
                tamaUltim = ultimInput.length,
                result = agregaCeros( ultimInput, tamaUltim );

            nomenclature = `{${nameCampo}-U(${result})}`;

        } else {

            let posInput = $('#posInputConf').val(),
                lengInput = $('#longInputConf').val(),
                tamaPos = posInput.length,
                tamaLeng = lengInput.length;


            let rUno = agregaCeros( posInput, tamaPos ),
                rDos = agregaCeros(lengInput, tamaLeng);
            // console.log(rUno)
            // console.log(rDos)

            nomenclature = `{${nameCampo}-D(${rUno},${rDos})}`;

        }

        if( $('#dividir').prop('checked') ){
            let str1 = nomenclature.split('}');

            nomenclature = `${str1[0]}/100}`;
        }

        $('#mensaje_alerta').val(`${contentTextA}${nomenclature}`);
        $('#longCampoSelect').html(``);
        $('#campoSelect').html(``);
        $('.content_config').find('input').val('');
        $('#dividir').attr('checked', false);
    }

});

//Evalua el cambios de los checkbox elegido
$(document).on('click', 'label.btn',function(){
    let input = $(this).children('input'),
        valInput = input.val();

    if( valInput == "0"){
        input.val('1')
    } else {
        input.val('0')
    }

});

//Boton guardar de .modal-dos mensaje de la modal
$(document).on('click', '#saveMsn', function(){
    let contAlert = $('.mensaje-de-alerta').children().length,
        mensajeAlerta = $('#mensaje_alerta').val(),
        fechaMsn = $('#fechaMsn').val(),
        desMsn = $('#desMsn').val(),
        escalaMsn = $('#escalaMsn').val(),
        limitesMsn = $('#limitesMsn').val(),
        referenciMsn = $('#referenciMsn').val(),
        id = $('#id_msn_alert').val();

    if( id == '' ){
        if( mensajeAlerta != ''){
            contAlert++;
            let trMsn = `<tr>
                        <td class="text-center mensajeInfo">${contAlert} <input type="hidden" name="msn[dataMsn${contAlert}][Num_Alarma]" value="${contAlert}"></td>
                        <td class="mensajeInfo">${mensajeAlerta} <input type="hidden" name="msn[dataMsn${contAlert}][message]" value="${mensajeAlerta}"></td>
                        <td class="mensajeInfo" style="display: none">${fechaMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_date]" value="${fechaMsn}"></td>
                        <td class="mensajeInfo" style="display: none">${desMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_description]" value="${desMsn}"></td>
                        <td class="mensajeInfo" style="display: none">${escalaMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_escala]" value="${escalaMsn}"></td>
                        <td class="mensajeInfo" style="display: none">${limitesMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_limit]" value="${limitesMsn}"></td>
                        <td class="mensajeInfo" style="display: none">${referenciMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_reference]" value="${referenciMsn}"></td>
                    </tr>`;

            $('.mensaje-de-alerta').append(trMsn);

            //resetear valores de la modal
            resetModal();

            //--Muestra modal principal y oculta la modal de crear mensaje
            $('.modal-dialog').fadeIn(300);
            $('.modal-dos').fadeOut(300);

            //--Eliminar color rojo de la tabla donde se muestran los mensajes
            $('.cont-tab-msn').removeClass('error-tables');

            if( contAlert == 2 ){

                $('.optional').css('display','block');
                $('#select_intermediate_time').val('0');
                $('#intermediate').val('').prop('required', true);23

            }
        }
    } else {
        // alert('Editare')
        //Este codigo reescribe el mensaje que ya existe del horario

        let tr = $(`tr#${id}`);

        // Cambiar el texto visible para
        tr.children('td').eq(2).html(`${mensajeAlerta}`);

        //Valor del input que lleva el mensaje
        tr.find('input#msn_edit').val(mensajeAlerta);

        // Valores para los bit_
        tr.find('input#fecha').val(fechaMsn);
        tr.find('input#descripcion').val(desMsn);
        tr.find('input#escala').val(escalaMsn);
        tr.find('input#limite').val(limitesMsn);
        tr.find('input#refecia').val(referenciMsn);

        //resetear valores de la modal y cerrarla
        resetModal();

    }
});

function nuevoFiltro(){
    // Id Usuario Logeado
    let valId = $('#userId').val();
    keyId = parseInt(valId);
    console.log(keyId);

    let valor = "";
    let td = $('.tBodyCondition').find('td.filterCondi');

    // Valores del Filtro
    let desFilter = $('#des_filter').val();
    let filterType = $('#filterType').val();

    if ( filterType != "0" && filterType != "" ){
        colorLabel = $('.minicolors-input').val();
    }

    td.each(function () {
        valor = $(this).html();
        condicionesFiltro.push(valor);
    });

    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: "/tx_filters/",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "_method": "create",
            idSession: idSession,
            nameFilter: desFilter,
            typeFilter: filterType,
            general: colorLabel,

            filtro: condicionesFiltro,
            keyId: keyId
        }),
        success: function () {

            swal({
                title: "Good job!",
                text: "Condition added!",
                type: "success"
            });

            window.location.href = '/tx_sessions/';
        }
    });

}

$(document).on("click", ".edFil", function (){

    let idFiltro = this.value; // Obtengo Id de filtro
    // alert(`Soy el filtro num: ${id}`);

    //
    let sessionId = $(`#filtro_${idFiltro}`).val();
    // alert(`Y pertenesco a Session_Id: ${sessionId}`);

    let sesionActive = $(`#sesion_${sessionId}`).val();

    if ( sesionActive == 1){
        swal({
            title: `${session_active_title}`,
            text: `${session_active_txt}`,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: `${btn_yes}`,
            cancelButtonText: `${btn_cancel}`,
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {

            if (isConfirm){

                swal({
                    title: `${session_active_title}`,
                    text: `${yourSure}`,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: `${btn_confir}`,
                    cancelButtonText: `${btn_cancel2}`,
                    closeOnConfirm: false,
                    closeOnCancel: false
                    },
                    function (isConfirm) {

                        if (isConfirm){

                            // Se envia el ajax para eliminar el filtro
                            // window.location.href = `/tx_filters/${id}/edit`
                            window.location.href = `/tx_filter_details/new?id=${idFiltro}`;

                        }
                        else{
                            swal(`${titleCanel}`,`${txtCancel}`, "error");
                        }
                    });

            } else {
                swal(`${titleCanel}`,`${txtCancel}`, "error");
            }
        });
    } else {
        window.location.href = `/tx_filter_details/new?id=${idFiltro}`;
    }
});

// Eliminar Subfiltros
$(document).on("click", ".eliminarFiltro", function (){

    let id = this.value;

    swal({
        title: `${swalDelete}`,
        text: `${dataWillLose}`,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: `${btn_yes}`,
        cancelButtonText: `${btn_cancel}`,
        closeOnConfirm: false,
        closeOnCancel: false
        },
        function (isConfirm) {

            if (isConfirm){

                swal({
                        title: `${swalDelete}`,
                        text: `${yourSure}`,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: `${btn_confir}`,
                        cancelButtonText: `${btn_cancel2}`,
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {

                        if (isConfirm){

                            // Se envia el ajax para eliminar el filtro
                            $.ajax({
                                type: "POST",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                },
                                url: "/tx_filters/" + id,
                                data: {"_method": "delete"}
                            })

                            setTimeout( function() {
                                swal( `${msnBtnDelete}` , "success");
                                window.location.href = '/tx_sessions/';
                            }, 500);



                        }
                        else{
                            swal(`${titleCanel}`,`${txtCancel}`, "error");
                        }
                    });

            } else {
                swal(`${titleCanel}`,`${txtCancel}`, "error");
            }

        });
});


//--Muestra input Main - General - Group de la ventana modal editar filtro
$(document).on('change', '#tx_filters_Order_number', function(){
    let _this = this,
        indexOption = _this.selectedIndex,
        type_filter = _this.options[indexOption].getAttribute('name');

    selectShow(type_filter)
});

//--Muestra input Main - General - Group para metodo NEW
let select_filtro = document.getElementById('typeFilter');
select_filtro.addEventListener('change', function(){

    let _this = this,
        indexOption = _this.selectedIndex,
        type_filter = _this.options[indexOption].getAttribute('name');

    selectShow(type_filter)

});

