require 'test_helper'

class TxScheduleReferencesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tx_schedule_reference = tx_schedule_references(:one)
  end

  test "should get index" do
    get tx_schedule_references_url
    assert_response :success
  end

  test "should get new" do
    get new_tx_schedule_reference_url
    assert_response :success
  end

  test "should create tx_schedule_reference" do
    assert_difference('TxScheduleReference.count') do
      post tx_schedule_references_url, params: { tx_schedule_reference: { field_formatId: @tx_schedule_reference.field_formatId, key_id: @tx_schedule_reference.key_id, schedule_id: @tx_schedule_reference.schedule_id, session_id: @tx_schedule_reference.session_id } }
    end

    assert_redirected_to tx_schedule_reference_url(TxScheduleReference.last)
  end

  test "should show tx_schedule_reference" do
    get tx_schedule_reference_url(@tx_schedule_reference)
    assert_response :success
  end

  test "should get edit" do
    get edit_tx_schedule_reference_url(@tx_schedule_reference)
    assert_response :success
  end

  test "should update tx_schedule_reference" do
    patch tx_schedule_reference_url(@tx_schedule_reference), params: { tx_schedule_reference: { field_formatId: @tx_schedule_reference.field_formatId, key_id: @tx_schedule_reference.key_id, schedule_id: @tx_schedule_reference.schedule_id, session_id: @tx_schedule_reference.session_id } }
    assert_redirected_to tx_schedule_reference_url(@tx_schedule_reference)
  end

  test "should destroy tx_schedule_reference" do
    assert_difference('TxScheduleReference.count', -1) do
      delete tx_schedule_reference_url(@tx_schedule_reference)
    end

    assert_redirected_to tx_schedule_references_url
  end
end
