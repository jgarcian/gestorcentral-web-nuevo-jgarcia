# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181210225717) do

  create_table "TR201809110004", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809120002", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809120003", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809120004", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809130002", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809130003", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809130004", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809140002", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809140003", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TR201809140004", force: :cascade do |t|
    t.integer  "procentual_filter_id"
    t.varchar  "time",                 limit: 10
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "TT201805290101", primary_key: "ID_TRAN", id: :decimal, precision: 18, scale: 0, force: :cascade do |t|
    t.datetime "FECHA_HORA_KM",                                          null: false
    t.char     "Rec_Type",           limit: 2
    t.char     "Ln_Tarj",            limit: 4
    t.char     "Fiid_Tarj",          limit: 4
    t.char     "Numero_de_Tarjeta",  limit: 19
    t.char     "PREFIJO6",           limit: 6
    t.char     "Ln_Comer",           limit: 4
    t.char     "Fiid_Comer",         limit: 4
    t.char     "Region",             limit: 4
    t.char     "ID_Comer",           limit: 19
    t.char     "Fiid_Term",          limit: 4
    t.char     "Term_ID_Term",       limit: 16
    t.char     "TIM",                limit: 8
    t.char     "REC_FRMT",           limit: 1
    t.char     "Tipo",               limit: 4
    t.char     "RTE_STAT",           limit: 2
    t.char     "ISS_CDE",            limit: 2
    t.char     "Entry_T",            limit: 16
    t.char     "Exit_T",             limit: 16
    t.char     "Re_Entry_T",         limit: 16
    t.char     "Fecha_Trans",        limit: 6
    t.char     "Hora_Trans",         limit: 8
    t.char     "Fecha_Posteo",       limit: 6
    t.char     "ACQ_ICHG_SETL_DAT",  limit: 6
    t.char     "ISS_ICHG_SETL_DAT",  limit: 6
    t.char     "Num_Sec",            limit: 12
    t.char     "TERM_NAME_LOC",      limit: 25
    t.char     "Nombre_de_terminal", limit: 22
    t.char     "Ciudad_terminal",    limit: 13
    t.char     "TERM_ST",            limit: 3
    t.char     "T_Cntry",            limit: 2
    t.char     "TERM_TIM_OFST",      limit: 2
    t.char     "Tipo_term",          limit: 2
    t.char     "Sic_Cde",            limit: 4
    t.char     "ORIG",               limit: 4
    t.char     "DEST",               limit: 4
    t.char     "Tipo_Transac",       limit: 2
    t.char     "Tarj_Asociada",      limit: 1
    t.char     "Cta_Asociada",       limit: 2
    t.char     "C",                  limit: 1
    t.char     "Tipo_Tarjeta",       limit: 2
    t.char     "Codigo_Respuesta",   limit: 3
    t.decimal  "Monto1",                         precision: 8, scale: 0
    t.decimal  "Monto2",                         precision: 8, scale: 0
    t.char     "Fecha_Exp",          limit: 4
    t.char     "INVOICE_NUM",        limit: 10
    t.char     "ORIG_INVOICE_NUM",   limit: 10
    t.char     "AUTH_IND",           limit: 1
    t.char     "SHIFT_NUM",          limit: 3
    t.char     "BATCH_SEQ_NUM",      limit: 3
    t.char     "Codigo_aprov",       limit: 8
    t.char     "SETL_FLAG",          limit: 1
    t.char     "Cod_reverso",        limit: 2
    t.char     "Entry_mode",         limit: 3
    t.char     "Moneda",             limit: 3
    t.char     "Currency",           limit: 3
    t.char     "SETL_CRNCY_CDE",     limit: 3
    t.char     "CRD_ACCPT_ID_NUM",   limit: 11
    t.char     "CRD_ISS_ID_NUM",     limit: 11
    t.char     "ORIG_TRAN_TIM",      limit: 8
    t.char     "OVRRDE_FLG",         limit: 1
    t.char     "ADDR",               limit: 20
    t.char     "ZIP_CDE",            limit: 9
    t.char     "ADDR_VRFY_STAT",     limit: 1
    t.char     "DAT",                limit: 6
    t.char     "TIM2",               limit: 8
    t.char     "PRE_AUTH_HLDS_LVL",  limit: 1
    t.char     "O",                  limit: 1
    t.char     "R",                  limit: 1
    t.char     "Dft_Capture",        limit: 1
    t.char     "PT_SRV_COND_CDE",    limit: 2
    t.char     "ORIG_TRAN_DAT",      limit: 4
    t.char     "EXCP_RSN_CDE",       limit: 3
    t.char     "Num_Inst_Emi",       limit: 11
    t.char     "RETAILER_ID",        limit: 19
    t.char     "Num_Inst_Adq",       limit: 11
    t.char     "INFO",               limit: 700
  end

  create_table "TT201805300101", primary_key: "ID_TRAN", id: :decimal, precision: 18, scale: 0, force: :cascade do |t|
    t.datetime "FECHA_HORA_KM",                                          null: false
    t.char     "Rec_Type",           limit: 2
    t.char     "Ln_Tarj",            limit: 4
    t.char     "Fiid_Tarj",          limit: 4
    t.char     "Numero_de_Tarjeta",  limit: 19
    t.char     "PREFIJO6",           limit: 6
    t.char     "Ln_Comer",           limit: 4
    t.char     "Fiid_Comer",         limit: 4
    t.char     "Region",             limit: 4
    t.char     "ID_Comer",           limit: 19
    t.char     "Fiid_Term",          limit: 4
    t.char     "Term_ID_Term",       limit: 16
    t.char     "TIM",                limit: 8
    t.char     "REC_FRMT",           limit: 1
    t.char     "Tipo",               limit: 4
    t.char     "RTE_STAT",           limit: 2
    t.char     "ISS_CDE",            limit: 2
    t.char     "Entry_T",            limit: 16
    t.char     "Exit_T",             limit: 16
    t.char     "Re_Entry_T",         limit: 16
    t.char     "Fecha_Trans",        limit: 6
    t.char     "Hora_Trans",         limit: 8
    t.char     "Fecha_Posteo",       limit: 6
    t.char     "ACQ_ICHG_SETL_DAT",  limit: 6
    t.char     "ISS_ICHG_SETL_DAT",  limit: 6
    t.char     "Num_Sec",            limit: 12
    t.char     "TERM_NAME_LOC",      limit: 25
    t.char     "Nombre_de_terminal", limit: 22
    t.char     "Ciudad_terminal",    limit: 13
    t.char     "TERM_ST",            limit: 3
    t.char     "T_Cntry",            limit: 2
    t.char     "TERM_TIM_OFST",      limit: 2
    t.char     "Tipo_term",          limit: 2
    t.char     "Sic_Cde",            limit: 4
    t.char     "ORIG",               limit: 4
    t.char     "DEST",               limit: 4
    t.char     "Tipo_Transac",       limit: 2
    t.char     "Tarj_Asociada",      limit: 1
    t.char     "Cta_Asociada",       limit: 2
    t.char     "C",                  limit: 1
    t.char     "Tipo_Tarjeta",       limit: 2
    t.char     "Codigo_Respuesta",   limit: 3
    t.decimal  "Monto1",                         precision: 8, scale: 0
    t.decimal  "Monto2",                         precision: 8, scale: 0
    t.char     "Fecha_Exp",          limit: 4
    t.char     "INVOICE_NUM",        limit: 10
    t.char     "ORIG_INVOICE_NUM",   limit: 10
    t.char     "AUTH_IND",           limit: 1
    t.char     "SHIFT_NUM",          limit: 3
    t.char     "BATCH_SEQ_NUM",      limit: 3
    t.char     "Codigo_aprov",       limit: 8
    t.char     "SETL_FLAG",          limit: 1
    t.char     "Cod_reverso",        limit: 2
    t.char     "Entry_mode",         limit: 3
    t.char     "Moneda",             limit: 3
    t.char     "Currency",           limit: 3
    t.char     "SETL_CRNCY_CDE",     limit: 3
    t.char     "CRD_ACCPT_ID_NUM",   limit: 11
    t.char     "CRD_ISS_ID_NUM",     limit: 11
    t.char     "ORIG_TRAN_TIM",      limit: 8
    t.char     "OVRRDE_FLG",         limit: 1
    t.char     "ADDR",               limit: 20
    t.char     "ZIP_CDE",            limit: 9
    t.char     "ADDR_VRFY_STAT",     limit: 1
    t.char     "DAT",                limit: 6
    t.char     "TIM2",               limit: 8
    t.char     "PRE_AUTH_HLDS_LVL",  limit: 1
    t.char     "O",                  limit: 1
    t.char     "R",                  limit: 1
    t.char     "Dft_Capture",        limit: 1
    t.char     "PT_SRV_COND_CDE",    limit: 2
    t.char     "ORIG_TRAN_DAT",      limit: 4
    t.char     "EXCP_RSN_CDE",       limit: 3
    t.char     "Num_Inst_Emi",       limit: 11
    t.char     "RETAILER_ID",        limit: 19
    t.char     "Num_Inst_Adq",       limit: 11
    t.char     "INFO",               limit: 700
  end

  create_table "TT201806130101", primary_key: "ID_TRAN", id: :decimal, precision: 18, scale: 0, force: :cascade do |t|
    t.datetime "FECHA_HORA_KM",                                          null: false
    t.char     "Rec_Type",           limit: 2
    t.char     "Ln_Tarj",            limit: 4
    t.char     "Fiid_Tarj",          limit: 4
    t.char     "Numero_de_Tarjeta",  limit: 19
    t.char     "PREFIJO6",           limit: 6
    t.char     "Ln_Comer",           limit: 4
    t.char     "Fiid_Comer",         limit: 4
    t.char     "Region",             limit: 4
    t.char     "ID_Comer",           limit: 19
    t.char     "Fiid_Term",          limit: 4
    t.char     "Term_ID_Term",       limit: 16
    t.char     "TIM",                limit: 8
    t.char     "REC_FRMT",           limit: 1
    t.char     "Tipo",               limit: 4
    t.char     "RTE_STAT",           limit: 2
    t.char     "ISS_CDE",            limit: 2
    t.char     "Entry_T",            limit: 16
    t.char     "Exit_T",             limit: 16
    t.char     "Re_Entry_T",         limit: 16
    t.char     "Fecha_Trans",        limit: 6
    t.char     "Hora_Trans",         limit: 8
    t.char     "Fecha_Posteo",       limit: 6
    t.char     "ACQ_ICHG_SETL_DAT",  limit: 6
    t.char     "ISS_ICHG_SETL_DAT",  limit: 6
    t.char     "Num_Sec",            limit: 12
    t.char     "TERM_NAME_LOC",      limit: 25
    t.char     "Nombre_de_terminal", limit: 22
    t.char     "Ciudad_terminal",    limit: 13
    t.char     "TERM_ST",            limit: 3
    t.char     "T_Cntry",            limit: 2
    t.char     "TERM_TIM_OFST",      limit: 2
    t.char     "Tipo_term",          limit: 2
    t.char     "Sic_Cde",            limit: 4
    t.char     "ORIG",               limit: 4
    t.char     "DEST",               limit: 4
    t.char     "Tipo_Transac",       limit: 2
    t.char     "Tarj_Asociada",      limit: 1
    t.char     "Cta_Asociada",       limit: 2
    t.char     "C",                  limit: 1
    t.char     "Tipo_Tarjeta",       limit: 2
    t.char     "Codigo_Respuesta",   limit: 3
    t.decimal  "Monto1",                         precision: 8, scale: 0
    t.decimal  "Monto2",                         precision: 8, scale: 0
    t.char     "Fecha_Exp",          limit: 4
    t.char     "INVOICE_NUM",        limit: 10
    t.char     "ORIG_INVOICE_NUM",   limit: 10
    t.char     "AUTH_IND",           limit: 1
    t.char     "SHIFT_NUM",          limit: 3
    t.char     "BATCH_SEQ_NUM",      limit: 3
    t.char     "Codigo_aprov",       limit: 8
    t.char     "SETL_FLAG",          limit: 1
    t.char     "Cod_reverso",        limit: 2
    t.char     "Entry_mode",         limit: 3
    t.char     "Moneda",             limit: 3
    t.char     "Currency",           limit: 3
    t.char     "SETL_CRNCY_CDE",     limit: 3
    t.char     "CRD_ACCPT_ID_NUM",   limit: 11
    t.char     "CRD_ISS_ID_NUM",     limit: 11
    t.char     "ORIG_TRAN_TIM",      limit: 8
    t.char     "OVRRDE_FLG",         limit: 1
    t.char     "ADDR",               limit: 20
    t.char     "ZIP_CDE",            limit: 9
    t.char     "ADDR_VRFY_STAT",     limit: 1
    t.char     "DAT",                limit: 6
    t.char     "TIM2",               limit: 8
    t.char     "PRE_AUTH_HLDS_LVL",  limit: 1
    t.char     "O",                  limit: 1
    t.char     "R",                  limit: 1
    t.char     "Dft_Capture",        limit: 1
    t.char     "PT_SRV_COND_CDE",    limit: 2
    t.char     "ORIG_TRAN_DAT",      limit: 4
    t.char     "EXCP_RSN_CDE",       limit: 3
    t.char     "Num_Inst_Emi",       limit: 11
    t.char     "RETAILER_ID",        limit: 19
    t.char     "Num_Inst_Adq",       limit: 11
    t.char     "INFO",               limit: 700
  end

  create_table "TT201806260101", primary_key: "ID_TRAN", id: :decimal, precision: 18, scale: 0, force: :cascade do |t|
    t.datetime "FECHA_HORA_KM",                                          null: false
    t.char     "Rec_Type",           limit: 2
    t.char     "Ln_Tarj",            limit: 4
    t.char     "Fiid_Tarj",          limit: 4
    t.char     "Numero_de_Tarjeta",  limit: 19
    t.char     "PREFIJO6",           limit: 6
    t.char     "Ln_Comer",           limit: 4
    t.char     "Fiid_Comer",         limit: 4
    t.char     "Region",             limit: 4
    t.char     "ID_Comer",           limit: 19
    t.char     "Fiid_Term",          limit: 4
    t.char     "Term_ID_Term",       limit: 16
    t.char     "TIM",                limit: 8
    t.char     "REC_FRMT",           limit: 1
    t.char     "Tipo",               limit: 4
    t.char     "RTE_STAT",           limit: 2
    t.char     "ISS_CDE",            limit: 2
    t.char     "Entry_T",            limit: 16
    t.char     "Exit_T",             limit: 16
    t.char     "Re_Entry_T",         limit: 16
    t.char     "Fecha_Trans",        limit: 6
    t.char     "Hora_Trans",         limit: 8
    t.char     "Fecha_Posteo",       limit: 6
    t.char     "ACQ_ICHG_SETL_DAT",  limit: 6
    t.char     "ISS_ICHG_SETL_DAT",  limit: 6
    t.char     "Num_Sec",            limit: 12
    t.char     "TERM_NAME_LOC",      limit: 25
    t.char     "Nombre_de_terminal", limit: 22
    t.char     "Ciudad_terminal",    limit: 13
    t.char     "TERM_ST",            limit: 3
    t.char     "T_Cntry",            limit: 2
    t.char     "TERM_TIM_OFST",      limit: 2
    t.char     "Tipo_term",          limit: 2
    t.char     "Sic_Cde",            limit: 4
    t.char     "ORIG",               limit: 4
    t.char     "DEST",               limit: 4
    t.char     "Tipo_Transac",       limit: 2
    t.char     "Tarj_Asociada",      limit: 1
    t.char     "Cta_Asociada",       limit: 2
    t.char     "C",                  limit: 1
    t.char     "Tipo_Tarjeta",       limit: 2
    t.char     "Codigo_Respuesta",   limit: 3
    t.decimal  "Monto1",                         precision: 8, scale: 0
    t.decimal  "Monto2",                         precision: 8, scale: 0
    t.char     "Fecha_Exp",          limit: 4
    t.char     "INVOICE_NUM",        limit: 10
    t.char     "ORIG_INVOICE_NUM",   limit: 10
    t.char     "AUTH_IND",           limit: 1
    t.char     "SHIFT_NUM",          limit: 3
    t.char     "BATCH_SEQ_NUM",      limit: 3
    t.char     "Codigo_aprov",       limit: 8
    t.char     "SETL_FLAG",          limit: 1
    t.char     "Cod_reverso",        limit: 2
    t.char     "Entry_mode",         limit: 3
    t.char     "Moneda",             limit: 3
    t.char     "Currency",           limit: 3
    t.char     "SETL_CRNCY_CDE",     limit: 3
    t.char     "CRD_ACCPT_ID_NUM",   limit: 11
    t.char     "CRD_ISS_ID_NUM",     limit: 11
    t.char     "ORIG_TRAN_TIM",      limit: 8
    t.char     "OVRRDE_FLG",         limit: 1
    t.char     "ADDR",               limit: 20
    t.char     "ZIP_CDE",            limit: 9
    t.char     "ADDR_VRFY_STAT",     limit: 1
    t.char     "DAT",                limit: 6
    t.char     "TIM2",               limit: 8
    t.char     "PRE_AUTH_HLDS_LVL",  limit: 1
    t.char     "O",                  limit: 1
    t.char     "R",                  limit: 1
    t.char     "Dft_Capture",        limit: 1
    t.char     "PT_SRV_COND_CDE",    limit: 2
    t.char     "ORIG_TRAN_DAT",      limit: 4
    t.char     "EXCP_RSN_CDE",       limit: 3
    t.char     "Num_Inst_Emi",       limit: 11
    t.char     "RETAILER_ID",        limit: 19
    t.char     "Num_Inst_Adq",       limit: 11
    t.char     "INFO",               limit: 700
  end

  create_table "Tbitobse", id: false, force: :cascade do |t|
    t.varchar  "Id_Usuario",          limit: 12,   null: false
    t.integer  "Id_Lay",                           null: false
    t.integer  "Id_Formato",                       null: false
    t.integer  "Id_Aplicacion",                    null: false
    t.integer  "Id_TipoReg",                       null: false
    t.string   "IP_Adress",           limit: 50,   null: false
    t.string   "PC_Name",             limit: 50,   null: false
    t.datetime "Fecha_Hora",                       null: false
    t.datetime "Fecha_Hora_Atencion",              null: false
    t.varchar  "Id_Usuario_Atencion", limit: 12
    t.string   "Observaciones",       limit: 1000
    t.string   "Status",              limit: 50
    t.datetime "Fecha_Hora_Mod"
  end

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "area_id"
  end

  create_table "categori_filters", force: :cascade do |t|
    t.integer  "id_categori"
    t.integer  "id_filter"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "data_types", force: :cascade do |t|
    t.string   "description"
    t.integer  "type_data"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "field_formats", force: :cascade do |t|
    t.integer  "format_id"
    t.integer  "lay_id"
    t.integer  "consecutive",      limit: 2
    t.integer  "fieldLay_id"
    t.integer  "position"
    t.integer  "length"
    t.integer  "field_type",       limit: 2
    t.integer  "keyFieldFormatId"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "field_lays", force: :cascade do |t|
    t.integer  "lay_id"
    t.string   "name"
    t.integer  "position",            limit: 2
    t.integer  "length",              limit: 2
    t.integer  "dataType",            limit: 2
    t.integer  "level",               limit: 2
    t.integer  "father",              limit: 2
    t.integer  "replaced",            limit: 2
    t.string   "alias"
    t.integer  "lengthField",         limit: 2
    t.integer  "keyFieldLayId"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "order_number_father"
  end

  create_table "filter_port_details", force: :cascade do |t|
    t.integer  "filterPort_id"
    t.integer  "conditionNumber"
    t.string   "connector"
    t.integer  "length"
    t.integer  "denied"
    t.string   "operator"
    t.string   "parenthesis"
    t.integer  "position"
    t.string   "value"
    t.integer  "fieldLay_id"
    t.integer  "key_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "filter_ports", force: :cascade do |t|
    t.integer  "format_id"
    t.integer  "lay_id"
    t.integer  "port"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "filtering_categorizations", force: :cascade do |t|
    t.string   "name_categorization"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "fitering_categorizations", force: :cascade do |t|
    t.string   "categorization_name"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "formatos", force: :cascade do |t|
    t.integer  "lay_id"
    t.string   "description"
    t.integer  "keyFormatId"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "gestor_settings", force: :cascade do |t|
    t.string   "blue_day"
    t.string   "blue_night"
    t.string   "yellow_day"
    t.string   "yellow_night"
    t.string   "red_day"
    t.string   "red_night"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "blue_day_end"
    t.string   "blue_night_end"
    t.string   "yellow_day_end"
    t.string   "yellow_night_end"
    t.string   "red_day_end"
    t.string   "red_night_end"
    t.string   "day_start"
    t.string   "day_end"
    t.string   "night_start"
    t.string   "night_end"
  end

  create_table "group_columns", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "column"
    t.string   "title"
    t.integer  "typeOperation"
    t.integer  "fieldFormat_id"
    t.integer  "typeChart"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "key_id"
    t.integer  "format_id"
    t.integer  "lay_id"
  end

  create_table "group_filter_details", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "conditionNumber"
    t.string   "connector"
    t.integer  "length"
    t.integer  "denied"
    t.string   "operator"
    t.string   "parenthesis"
    t.integer  "position"
    t.string   "value"
    t.integer  "fieldFormat_id"
    t.string   "key_id"
    t.integer  "lay_id"
    t.integer  "format_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "group_filters", force: :cascade do |t|
    t.integer  "group_id"
    t.string   "description"
    t.string   "backGroundColor"
    t.string   "letterColor"
    t.integer  "orderNumber"
    t.integer  "key_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "group_lays", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "orderNumber"
    t.integer  "fieldFormat_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "key_id"
    t.integer  "format_id"
    t.integer  "lay_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "detail"
    t.string   "reference"
    t.string   "area"
    t.string   "hist_alert"
    t.string   "hist_trans"
    t.boolean  "fraude"
    t.boolean  "inactive"
    t.boolean  "actualizar"
  end

  create_table "lays", force: :cascade do |t|
    t.string   "description"
    t.integer  "keyLayId"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "mail_groups", force: :cascade do |t|
    t.integer  "group_id"
    t.string   "correo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "old_passwords", force: :cascade do |t|
    t.string   "encrypted_password",       null: false
    t.string   "password_archivable_type", null: false
    t.integer  "password_archivable_id",   null: false
    t.datetime "created_at"
    t.string   "password_salt"
  end

  create_table "operation_types", force: :cascade do |t|
    t.string   "description"
    t.integer  "typeOperation"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.integer  "view_id"
    t.string   "view_name"
    t.integer  "crear"
    t.integer  "editar"
    t.integer  "leer"
    t.integer  "eliminar"
    t.integer  "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "porcentual_filter_details", force: :cascade do |t|
    t.integer  "porcentual_filter_id"
    t.integer  "field_format_id"
    t.integer  "conditionNumber"
    t.string   "connector"
    t.integer  "length"
    t.integer  "denied"
    t.string   "operator"
    t.string   "parenthesis"
    t.integer  "position"
    t.string   "value"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "key_id"
    t.string   "token_id"
    t.index ["field_format_id"], name: "index_porcentual_filter_details_on_field_format_id"
    t.index ["porcentual_filter_id"], name: "index_porcentual_filter_details_on_porcentual_filter_id"
  end

  create_table "porcentual_filters", force: :cascade do |t|
    t.integer  "porcentual_session_id"
    t.integer  "group_id"
    t.string   "description"
    t.string   "backgroundColor"
    t.string   "letterColor"
    t.integer  "order_number"
    t.integer  "key_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["porcentual_session_id"], name: "index_porcentual_filters_on_porcentual_session_id"
  end

  create_table "porcentual_groups", force: :cascade do |t|
    t.integer  "lay_id"
    t.integer  "format_id"
    t.string   "description"
    t.boolean  "inactive"
    t.boolean  "actualizar"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "porcentual_sessions", force: :cascade do |t|
    t.integer  "format_id"
    t.integer  "lay_id"
    t.string   "description"
    t.string   "sessionColor"
    t.integer  "interval"
    t.integer  "key_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "inactive"
    t.integer  "actualizar"
    t.index ["format_id"], name: "index_porcentual_sessions_on_format_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "name"
    t.integer  "flag"
    t.integer  "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "day"
    t.integer  "month"
  end

  create_table "referenciadealerta", force: :cascade do |t|
    t.integer  "idAlerta"
    t.integer  "idCampo"
    t.integer  "idProducto"
    t.string   "campo"
    t.string   "valor"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.string   "mensDefault"
    t.integer  "fraude"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "IdEstado"
    t.integer  "falso_positivo"
  end

  create_table "table_orders", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name_column"
    t.integer  "order"
    t.string   "table_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "table_transacciones_de_alerta", force: :cascade do |t|
  end

  create_table "talerta", primary_key: ["IdGrupo", "IdPerfil", "IdAlerta"], force: :cascade do |t|
    t.integer "IdGrupo",     limit: 2,                null: false
    t.integer "IdPerfil",    limit: 2,                null: false
    t.integer "IdAlerta",    limit: 2,                null: false
    t.time    "HoraIni",                precision: 7
    t.time    "HoraFin",                precision: 7
    t.varchar "Mascara",     limit: 7
    t.varchar "Mensaje",     limit: 60
    t.integer "IdPrioridad", limit: 2,                null: false
  end

  create_table "talertaxusr", primary_key: ["IdGrupo", "IdPerfil", "IdAlerta", "Prioridad", "IdUsuario"], force: :cascade do |t|
    t.integer "IdGrupo",   limit: 2,  null: false
    t.integer "IdPerfil",  limit: 2,  null: false
    t.integer "IdAlerta",  limit: 2,  null: false
    t.integer "Prioridad", limit: 2,  null: false
    t.varchar "IdUsuario", limit: 12, null: false
  end

  create_table "taplicac", primary_key: "Id_Aplicacion", force: :cascade do |t|
    t.integer    "Ssid",          limit: 2,          null: false
    t.text_basic "Descripcion",   limit: 2147483647
    t.text_basic "Ubicacion",     limit: 2147483647
    t.varchar    "Version",       limit: 10
    t.datetime   "Fecha_Version"
  end

  create_table "taplicaciones", primary_key: "IdAplicacion", force: :cascade do |t|
    t.varchar "Nombre",           limit: 60
    t.varchar "Descripcion",      limit: 60
    t.varchar "Version",          limit: 60
    t.varchar "FechaInstalacion", limit: 60
    t.integer "SVR",                         null: false
  end

  create_table "taplicacioneskm", force: :cascade do |t|
    t.integer  "IdAplicacion"
    t.string   "Nombre"
    t.string   "Descripcion"
    t.string   "Version"
    t.integer  "idProducto"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taplicacionesxusr", primary_key: ["IdUsuario", "IdAplicacion"], force: :cascade do |t|
    t.varchar "IdUsuario",    limit: 12, null: false
    t.integer "IdAplicacion", limit: 2,  null: false
    t.integer "Editable",                null: false
  end

  create_table "tasignacionxusr", primary_key: ["IdGrupo", "IdUsuario", "IdTran"], force: :cascade do |t|
    t.integer "IdGrupo",         limit: 2,   null: false
    t.varchar "IdUsuario",       limit: 12,  null: false
    t.varchar "IdTran",          limit: 100, null: false
    t.varchar "FechaAsignacion", limit: 10,  null: false
  end

  create_table "tatendidasxhoy", force: :cascade do |t|
    t.string   "fecha"
    t.integer  "idProducto"
    t.integer  "cantidad"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tatendiendos", force: :cascade do |t|
    t.varchar  "IdUsuario",  limit: 255
    t.varchar  "IdTran",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tbitacora", primary_key: "Id", force: :cascade do |t|
    t.integer    "IdProducto",    limit: 2
    t.varchar    "IdUsuario",     limit: 12
    t.integer    "IdLay",         limit: 2
    t.integer    "IdFormato",     limit: 2
    t.integer    "IdGrupo",       limit: 2
    t.integer    "IdPerfil",      limit: 2
    t.integer    "IdAlerta",      limit: 2
    t.integer    "IdFiltro",      limit: 2
    t.integer    "IdAplicacion",  limit: 2
    t.integer    "IdTipoReg",     limit: 2
    t.varchar    "IP",            limit: 39
    t.varchar    "NombrePC",      limit: 15
    t.varchar    "Fecha",         limit: 10
    t.varchar    "Hora",          limit: 14
    t.varchar    "IdTran",        limit: 80
    t.text_basic "Comparacion",   limit: 2147483647
    t.integer    "Escalamiento",  limit: 2
    t.text_basic "Mensaje",       limit: 2147483647
    t.integer    "Limite"
    t.integer    "Tiempo"
    t.text_basic "Referencia",    limit: 2147483647
    t.text_basic "Condicion",     limit: 2147483647
    t.integer    "IdEstado",      limit: 2
    t.integer    "IdPrioridad",   limit: 2
    t.integer    "FalsoPositivo", limit: 2
    t.integer    "FactorRiesgo"
    t.integer    "IdProblema"
    t.integer    "canceladas",    limit: 2
  end

  create_table "tbitaobse", primary_key: "Id", force: :cascade do |t|
    t.integer "IdAlerta",                       null: false
    t.varchar "Fecha",             limit: 10,   null: false
    t.varchar "Hora",              limit: 14,   null: false
    t.varchar "Observaciones",     limit: 1000, null: false
    t.integer "IdEstado",          limit: 2,    null: false
    t.varchar "IdUsuarioAtencion", limit: 12,   null: false
    t.integer "falso_positivo"
  end

  create_table "tbitctrl", primary_key: "IdTipoReg", force: :cascade do |t|
    t.integer "Periodo",      limit: 2, null: false
    t.integer "UnidadTiempo", limit: 2, null: false
    t.integer "Activo",                 null: false
  end

  create_table "tcamposformato", primary_key: ["IdLay", "IdFormato", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",         limit: 2, null: false
    t.integer "IdFormato",     limit: 2, null: false
    t.integer "IdConsecutivo", limit: 2, null: false
    t.integer "IdCampo",       limit: 2, null: false
    t.integer "TipoCampo",     limit: 2, null: false
  end

  create_table "tcamposlay", primary_key: ["IdLay", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",         limit: 2,  null: false
    t.integer "IdCampo",       limit: 2,  null: false
    t.varchar "Nombre",        limit: 45, null: false
    t.integer "Posicion",      limit: 2,  null: false
    t.integer "Longitud",      limit: 2,  null: false
    t.integer "TipoDato",      limit: 2,  null: false
    t.integer "Nivel",         limit: 2,  null: false
    t.integer "Padre",                    null: false
    t.integer "SeSustituye",              null: false
    t.varchar "Alias",         limit: 45, null: false
    t.integer "CampoLongitud", limit: 2,  null: false
  end

  create_table "tcampostoken", primary_key: ["IdLay", "IdFormato", "IdToken", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",     limit: 2, null: false
    t.integer "IdFormato", limit: 2, null: false
    t.varchar "IdToken",   limit: 5, null: false
    t.integer "IdCampo",   limit: 2, null: false
  end

  create_table "tcamposxgrupo", primary_key: ["IdGrupo", "IdConsecutivo"], force: :cascade do |t|
    t.integer "IdGrupo",       limit: 2, null: false
    t.integer "IdConsecutivo", limit: 2, null: false
    t.integer "IdCampo",       limit: 2, null: false
    t.varchar "IdToken",       limit: 5
  end

  create_table "tcategoria", primary_key: "IdCategoria", force: :cascade do |t|
    t.varchar "Descripcion", limit: 60, null: false
    t.integer "Valor",                  null: false
  end

  create_table "tcategoriaxalerta", primary_key: ["IdCategoria", "IdPerfil", "IdGrupo"], force: :cascade do |t|
    t.integer "IdCategoria", limit: 2, null: false
    t.integer "IdPerfil",    limit: 2, null: false
    t.integer "IdGrupo",     limit: 2, null: false
  end

  create_table "tcolxgpo", primary_key: ["IdGrupo", "Columna"], force: :cascade do |t|
    t.integer "IdGrupo",     limit: 2,  null: false
    t.integer "Columna",     limit: 2,  null: false
    t.varchar "Titulo",      limit: 60
    t.integer "IdTipoCampo", limit: 2
    t.integer "IdCampo",     limit: 2
  end

  create_table "tconfigs", force: :cascade do |t|
    t.integer  "section"
    t.string   "value"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tdefoperacion", primary_key: ["IdGrupo", "IdOperacion", "IdCondicion"], force: :cascade do |t|
    t.integer "IdGrupo",         limit: 2,  null: false
    t.integer "IdOperacion",     limit: 2,  null: false
    t.integer "IdCondicion",     limit: 2,  null: false
    t.varchar "Operacion",       limit: 50
    t.integer "IdTipoOperador",  limit: 2
    t.integer "IdTipoOperacion", limit: 2
    t.integer "Columna",         limit: 2
    t.varchar "Valor",           limit: 50
    t.varchar "Parentesis",      limit: 1
  end

  create_table "tdeftoken", primary_key: ["IdLay", "IdToken", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",          limit: 2,  null: false
    t.varchar "IdToken",        limit: 5,  null: false
    t.integer "IdCampo",        limit: 2,  null: false
    t.varchar "Nombre",         limit: 50
    t.integer "Longitud",       limit: 2
    t.integer "TipoDato",       limit: 2
    t.integer "LongDespliegue", limit: 2
    t.varchar "Alias",          limit: 50
    t.integer "Posicion"
  end

  create_table "tdtokens", primary_key: ["Id_Lay", "Id_Token", "Id_Campo"], force: :cascade do |t|
    t.integer "Id_Lay",          limit: 2,  null: false
    t.varchar "Id_Token",        limit: 5,  null: false
    t.integer "Id_Campo",        limit: 2,  null: false
    t.varchar "Nombre",          limit: 50
    t.integer "Longitud",        limit: 2
    t.integer "Tipo_dato",       limit: 2
    t.integer "Long_Despliegue", limit: 2
    t.varchar "Alias",           limit: 50
    t.integer "Posicion"
  end

  create_table "tequivalenciaxc", primary_key: ["IdLay", "IdCampo", "Consecutivo"], force: :cascade do |t|
    t.integer "IdLay",        limit: 2,  null: false
    t.integer "IdCampo",      limit: 2,  null: false
    t.integer "Consecutivo",  limit: 2,  null: false
    t.integer "Longitud",     limit: 2
    t.varchar "Valor",        limit: 60, null: false
    t.varchar "Equivalencia", limit: 60, null: false
  end

  create_table "tescalamientos", primary_key: ["IdGrupo", "IdPerfil", "IdAlerta", "IdEstado", "IdEscalamiento", "IdUsrCorreo"], force: :cascade do |t|
    t.integer "IdGrupo",        limit: 2,   null: false
    t.integer "IdPerfil",       limit: 2,   null: false
    t.integer "IdAlerta",       limit: 2,   null: false
    t.integer "IdEstado",       limit: 2,   null: false
    t.integer "IdEscalamiento", limit: 2,   null: false
    t.varchar "IdUsrCorreo",    limit: 45,  null: false
    t.varchar "Mensaje",        limit: 100
    t.integer "Tiempo",         limit: 2
    t.integer "Unidad",         limit: 2
  end

  create_table "testadosxalerta", primary_key: "IdEstado", force: :cascade do |t|
    t.varchar "Descripcion",    limit: 60, null: false
    t.integer "falso_positivo"
    t.string  "mensajeDefault"
  end

  create_table "testadosxusr", primary_key: "IdEstado", force: :cascade do |t|
    t.varchar "Descripcion", limit: 60, null: false
  end

  create_table "tfiltrosxgrupo", primary_key: ["IdGrupo", "IdFiltro"], force: :cascade do |t|
    t.integer "IdGrupo",    limit: 2,   null: false
    t.integer "IdFiltro",   limit: 2,   null: false
    t.varchar "Condicion",  limit: 255
    t.varchar "Operador",   limit: 60
    t.varchar "Valor1",     limit: 255
    t.varchar "Parentesis", limit: 20,  null: false
    t.integer "Negacion",               null: false
    t.varchar "valor2",     limit: 255
    t.integer "IdCampo",    limit: 2,   null: false
    t.varchar "IdToken",    limit: 5
    t.varchar "Conector",   limit: 10
    t.varchar "Query",      limit: 255, null: false
    t.varchar "TipoDato",   limit: 255
  end

  create_table "tfiltroxperfil", primary_key: ["IdGrupo", "IdPerfil", "IdCondicion"], force: :cascade do |t|
    t.integer "IdGrupo",         limit: 2,   null: false
    t.integer "IdPerfil",        limit: 2,   null: false
    t.integer "IdCondicion",     limit: 2,   null: false
    t.varchar "Condicion",       limit: 300
    t.varchar "Operador",        limit: 20
    t.varchar "Parentesis",      limit: 10
    t.integer "Negacion"
    t.varchar "Conector",        limit: 45
    t.integer "Comportamiento1", limit: 2
    t.integer "Columna1",        limit: 2
    t.varchar "TipoFuncion1",    limit: 45
    t.integer "Comportamiento2", limit: 2
    t.integer "Columna2",        limit: 2
    t.varchar "TipoFuncion2",    limit: 45
    t.varchar "Valor",           limit: 100
    t.integer "Porcentaje",      limit: 2
  end

  create_table "tfmtxusr", primary_key: ["Id_Usuario", "Id_Lay", "Id_Formato"], force: :cascade do |t|
    t.varchar    "Id_Usuario", limit: 12,         null: false
    t.integer    "Id_Lay",     limit: 2,          null: false
    t.integer    "Id_Formato", limit: 2,          null: false
    t.text_basic "Mascara",    limit: 2147483647
  end

  create_table "tformatos", primary_key: ["IdLay", "IdFormato"], force: :cascade do |t|
    t.integer "IdLay",       limit: 2,  null: false
    t.integer "IdFormato",   limit: 2,  null: false
    t.varchar "Descripcion", limit: 60, null: false
  end

  create_table "tformatosxusr", primary_key: ["IdUsuario", "IdLay", "IdFormato", "IdCampo"], force: :cascade do |t|
    t.varchar "IdUsuario", limit: 12, null: false
    t.integer "IdLay",     limit: 2,  null: false
    t.integer "IdFormato", limit: 2,  null: false
    t.integer "IdCampo",   limit: 2,  null: false
  end

  create_table "tgiros", primary_key: "IdGiro", id: :varchar, limit: 500, force: :cascade do |t|
    t.integer "Valor", null: false
  end

  create_table "tgposctrl", primary_key: "IdGrupo", force: :cascade do |t|
    t.integer "Periodo",         limit: 2,  null: false
    t.integer "UnidadTiempo",    limit: 2,  null: false
    t.varchar "UltimaEjecucion", limit: 20
    t.varchar "FechaEliminada",  limit: 20
    t.integer "Registros"
    t.integer "Activo",                     null: false
  end

  create_table "tgrupos", primary_key: "IdGrupo", force: :cascade do |t|
    t.integer "IdLay",          limit: 2,   null: false
    t.integer "IdFormato",      limit: 2,   null: false
    t.varchar "Nombre",         limit: 60,  null: false
    t.varchar "CadenaConexion", limit: 200
    t.varchar "Tabla",          limit: 60
    t.varchar "Campo",          limit: 60
    t.integer "Funcion",        limit: 2,   null: false
    t.integer "Activo",                     null: false
    t.integer "cancelacion",    limit: 2
  end

  create_table "thistorico", primary_key: ["IdUsuario", "Fecha", "Contrasena"], force: :cascade do |t|
    t.varchar "IdUsuario",  limit: 12, null: false
    t.date    "Fecha",                 null: false
    t.varchar "Contrasena", limit: 60, null: false
  end

  create_table "thistoricoxperfil", primary_key: ["IdGrupo", "IdPerfil"], force: :cascade do |t|
    t.integer "IdGrupo",              limit: 2, null: false
    t.integer "IdPerfil",             limit: 2, null: false
    t.integer "TipoActual",           limit: 2
    t.integer "RangoActual",          limit: 2
    t.integer "UnidadActual",         limit: 2
    t.integer "InicialActual",        limit: 2
    t.integer "FinalActual",          limit: 2
    t.integer "TipoHistorico",        limit: 2
    t.integer "RangoHistorico",       limit: 2
    t.integer "UnidadHistorico",      limit: 2
    t.integer "InicialHistorico",     limit: 2
    t.integer "FinalHistorico",       limit: 2
    t.integer "Desplazamiento"
    t.integer "TipoDesplazamiento",   limit: 2
    t.integer "RangoDesplazamiento",  limit: 2
    t.integer "UnidadDesplazamiento", limit: 2
  end

  create_table "tlay", primary_key: "IdLay", force: :cascade do |t|
    t.varchar "Descripcion", limit: 36
  end

  create_table "tlaylogc", primary_key: ["Id_Lay", "Id_Campo"], force: :cascade do |t|
    t.integer "Id_Lay",        limit: 2,  null: false
    t.integer "Id_Campo",      limit: 2,  null: false
    t.varchar "Nombre_Campo",  limit: 36
    t.integer "Posicion",      limit: 2
    t.integer "Longitud",      limit: 2
    t.integer "Tipo_Dato"
    t.integer "Nivel",         limit: 2
    t.integer "Padre",         limit: 2
    t.integer "Se_Sustituye"
    t.varchar "Alias",         limit: 36
    t.integer "Camp_Longitud", limit: 2
  end

  create_table "tlaylogcs", primary_key: ["Id_Lay", "Id_Campo"], force: :cascade do |t|
    t.integer "Id_Lay",        limit: 2,  null: false
    t.integer "Id_Campo",      limit: 2,  null: false
    t.varchar "Nombre_Campo",  limit: 36
    t.integer "Posicion",      limit: 2
    t.integer "Longitud",      limit: 2
    t.integer "Tipo_Dato"
    t.integer "Nivel",         limit: 2
    t.integer "Padre",         limit: 2
    t.integer "Se_Sustituye"
    t.varchar "Alias",         limit: 36
    t.integer "Camp_Longitud", limit: 2
  end

  create_table "tlaylogt", primary_key: "Id_Lay", force: :cascade do |t|
    t.varchar "Descripcion", limit: 60
    t.integer "Num_Campos"
    t.varchar "Mascara",     limit: 5000
  end

  create_table "tlaylogts", primary_key: "Id_Lay", force: :cascade do |t|
    t.varchar    "Descripcion", limit: 60
    t.integer    "Num_Campos",  limit: 2
    t.text_basic "Mascara",     limit: 2147483647
  end

  create_table "tlaymsgt", primary_key: ["Id_Lay", "Id_Formato"], force: :cascade do |t|
    t.integer    "Id_Lay",      limit: 2,          null: false
    t.integer    "Id_Formato",  limit: 2,          null: false
    t.varchar    "Descripcion", limit: 60
    t.text_basic "Mascara",     limit: 2147483647
  end

  create_table "tlaymsgts", primary_key: ["Id_Lay", "Id_Formato"], force: :cascade do |t|
    t.integer    "Id_Lay",      limit: 2,          null: false
    t.integer    "Id_Formato",  limit: 2,          null: false
    t.varchar    "Descripcion", limit: 60
    t.text_basic "Mascara",     limit: 2147483647
  end

  create_table "tmontos", primary_key: ["MontoInicial", "MontoFinal"], force: :cascade do |t|
    t.integer "MontoInicial", null: false
    t.integer "MontoFinal",   null: false
    t.integer "Valor",        null: false
  end

  create_table "tmtokens", primary_key: ["Id_Lay", "Id_Token"], force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,  null: false
    t.varchar "Id_Token",    limit: 5,  null: false
    t.integer "Long_Id",     limit: 2
    t.integer "Long_Total",  limit: 2
    t.integer "Num_Campos",  limit: 2
    t.varchar "Descripcion", limit: 50
  end

  create_table "tperfiles", primary_key: ["IdGrupo", "IdPerfil"], force: :cascade do |t|
    t.integer "IdGrupo",  limit: 2,  null: false
    t.integer "IdPerfil", limit: 2,  null: false
    t.varchar "Nombre",   limit: 60
    t.varchar "FechaReg", limit: 10
    t.integer "Activo"
    t.integer "Batch"
  end

  create_table "tprevio", primary_key: "IdRegla", force: :cascade do |t|
    t.varchar "Tarjeta",         limit: 19
    t.varchar "FIID",            limit: 4
    t.varchar "Afiliacion",      limit: 19
    t.varchar "Ciudad",          limit: 13
    t.varchar "Giro",            limit: 45
    t.varchar "Pais",            limit: 2
    t.varchar "ModoEntrada",     limit: 3
    t.varchar "CodigoCondicion", limit: 2
    t.varchar "Monto",           limit: 8
    t.integer "Transacciones"
    t.integer "Operador",        limit: 2
    t.integer "ValidarLimites"
    t.varchar "Maximo",          limit: 8
    t.varchar "Minimo",          limit: 8
    t.integer "Accion",          limit: 2
    t.integer "Activo"
  end

  create_table "tprioridades", primary_key: "IdPrioridad", force: :cascade do |t|
    t.varchar "Descripcion", limit: 60, null: false
  end

  create_table "tprivilegios", primary_key: "IdPrivilegio", force: :cascade do |t|
    t.varchar "Descripcion", limit: 60, null: false
  end

  create_table "tproblemas", primary_key: "IdProblema", force: :cascade do |t|
    t.varchar "Descripcion", limit: 100
  end

  create_table "transacciones_de_alerta", force: :cascade do |t|
    t.integer "idAlerta"
    t.integer "idTran"
    t.string  "ttTableName"
    t.integer "transaction_type"
  end

  create_table "transactional_search_details", force: :cascade do |t|
    t.integer  "field_lay_id"
    t.string   "operator"
    t.string   "value"
    t.string   "parenthesis"
    t.string   "connector"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "user_id"
    t.integer  "key_id"
    t.integer  "transactional_search_id"
    t.integer  "conditionnumber"
  end

  create_table "transactional_searches", force: :cascade do |t|
    t.string   "description"
    t.datetime "initial_date"
    t.datetime "end_date"
    t.string   "data"
    t.integer  "layout_id"
    t.integer  "format_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "cards_id"
    t.integer  "transacction_type_id"
    t.integer  "plan_id"
    t.integer  "promotion_id"
    t.integer  "affiliation_id"
    t.integer  "card_id"
    t.varchar  "name",                 limit: 255
    t.integer  "amount"
    t.integer  "amount2"
    t.integer  "approval"
    t.integer  "response_code"
    t.varchar  "address",              limit: 255
    t.integer  "deferral"
    t.integer  "partial"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "trcestatus", primary_key: "Id", force: :cascade do |t|
    t.varchar "Fecha",                  limit: 10
    t.varchar "Hora",                   limit: 12
    t.bigint  "BloquesRecibidos"
    t.bigint  "TransaccionesRecibidas"
    t.integer "Desconexiones"
  end

  create_table "trecuperaciones", primary_key: "IdGrupo", force: :cascade do |t|
    t.varchar "FechaInicial",    limit: 10, null: false
    t.varchar "FechaRecuperada", limit: 10
    t.varchar "FechaEjecucion",  limit: 10
    t.integer "Registros"
    t.integer "Activo"
  end

  create_table "treferenciaxgpo", primary_key: ["IdGrupo", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",     limit: 2, null: false
    t.integer "IdFormato", limit: 2, null: false
    t.integer "IdGrupo",   limit: 2, null: false
    t.integer "IdCampo",   limit: 2, null: false
    t.varchar "IdToken",   limit: 5
  end

  create_table "ttipocampo", primary_key: "IdTipoCampo", force: :cascade do |t|
    t.varchar "Descripcion", limit: 50
  end

  create_table "ttipooperacion", primary_key: "IdTipoOperacion", force: :cascade do |t|
    t.varchar "Descripcion", limit: 50
  end

  create_table "ttipooperador", primary_key: "IdTipoOperador", force: :cascade do |t|
    t.varchar "Descripcion", limit: 50
  end

  create_table "ttiporegalarma", primary_key: "IdTipoReg", force: :cascade do |t|
    t.varchar "Descripcion", limit: 50
  end

  create_table "ttkneqxc", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,  null: false
    t.varchar "Id_Token",    limit: 10, null: false
    t.integer "Id_Campo",    limit: 2,  null: false
    t.integer "Consecutivo", limit: 2,  null: false
    t.integer "Longitud",    limit: 2
    t.varchar "Valor",       limit: 50
    t.varchar "Equivalente", limit: 50
  end

  create_table "ttknmsgt", primary_key: ["Id_Lay", "Id_Formato", "Id_Token", "Id_Campo"], force: :cascade do |t|
    t.integer "Id_Lay",       limit: 2,  null: false
    t.integer "Id_Formato",   limit: 2,  null: false
    t.varchar "Id_Token",     limit: 5,  null: false
    t.integer "Id_Campo",     limit: 2,  null: false
    t.integer "Tipo_Dato",    limit: 2
    t.integer "Longitud",     limit: 2
    t.varchar "Nombre_Campo", limit: 50
  end

  create_table "ttknxusr", primary_key: ["Id_Lay", "Id_Formato", "Id_Usuario", "Id_Token", "Id_Campo"], force: :cascade do |t|
    t.integer "Id_Lay",     limit: 2,  null: false
    t.integer "Id_Formato", limit: 2,  null: false
    t.varchar "Id_Usuario", limit: 12, null: false
    t.varchar "Id_Token",   limit: 5,  null: false
    t.integer "Id_Campo",   limit: 2,  null: false
  end

  create_table "ttoken", primary_key: ["IdLay", "IdToken"], force: :cascade do |t|
    t.integer "IdLay",       limit: 2,   null: false
    t.varchar "IdToken",     limit: 5,   null: false
    t.integer "LongId",      limit: 2
    t.integer "LongTotal",   limit: 2
    t.integer "NumCampos",   limit: 2
    t.varchar "Descripcion", limit: 100
  end

  create_table "tultimaaccion", primary_key: "UltimaAccion", force: :cascade do |t|
    t.varchar "Descripcion", limit: 60
  end

  create_table "tuserskm", primary_key: "Id_Usuario", id: :varchar, limit: 12, force: :cascade do |t|
    t.integer  "Id_Grupo",         limit: 2,  null: false
    t.integer  "Estado",           limit: 2
    t.integer  "Filtra_Evento",    limit: 2
    t.integer  "Ultima_Accion",    limit: 2
    t.datetime "Fec_Vig_Usuario"
    t.datetime "Fec_Vig_Clave"
    t.datetime "Fec_Hora_Ult_Acc"
    t.datetime "Fec_Hora_Ult_Opr"
    t.varchar  "Contrasena",       limit: 60
    t.varchar  "Nombre",           limit: 60
    t.varchar  "Ultima_Operacion", limit: 50
    t.datetime "Fec_Ini_Usuario"
    t.datetime "Fec_Ini_Clave"
  end

  create_table "tusrcorreos", primary_key: "IdUsrCorreo", id: :varchar, limit: 45, force: :cascade do |t|
    t.varchar "Nombre",   limit: 60, null: false
    t.varchar "correo",   limit: 60, null: false
    t.varchar "Telefono", limit: 60
  end

  create_table "tx_filter_details", force: :cascade do |t|
    t.integer "filter_id"
    t.integer "conditionnumber"
    t.varchar "connector",       limit: 45
    t.varchar "denied",          limit: 45
    t.varchar "operator",        limit: 45
    t.varchar "parenthesis",     limit: 45
    t.varchar "value",           limit: 500
    t.integer "fieldFormat_id"
    t.integer "position"
    t.integer "lenght"
    t.integer "key_id"
    t.string  "token_id"
  end

  create_table "tx_filter_group_fields", force: :cascade do |t|
    t.integer "filter_id"
    t.integer "field_formatid"
    t.varchar "name",           limit: 200
    t.integer "position"
    t.integer "length"
    t.integer "type_data",      limit: 2
    t.integer "nivel"
    t.integer "replace",        limit: 2
  end

  create_table "tx_filter_schedules", force: :cascade do |t|
    t.integer "filter_id"
    t.varchar "Hr_Start",               limit: 45
    t.varchar "Hr_End",                 limit: 45
    t.integer "Max_Tran"
    t.integer "Min_Tran"
    t.integer "Interval"
    t.integer "UMInterval"
    t.integer "Intermediate"
    t.integer "UMIntermediate"
    t.integer "TimeOut"
    t.integer "UMTimeout"
    t.integer "Accumulated_Amount"
    t.varchar "Type_Evaluation_Amount", limit: 1
  end

  create_table "tx_filters", primary_key: "Id", force: :cascade do |t|
    t.integer "Session_id"
    t.integer "Group_id"
    t.varchar "Description",     limit: 400
    t.integer "Lettercolor"
    t.integer "Order_number"
    t.integer "Key_id"
    t.string  "Backgroundcolor"
    t.integer "Inactive"
  end

  create_table "tx_formulaxcol", force: :cascade do |t|
    t.varchar    "user_id",      limit: 12
    t.integer    "Lay_id"
    t.integer    "Format_id"
    t.integer    "Order"
    t.varchar    "Description",  limit: 50
    t.text_basic "Formula",      limit: 2147483647
    t.text_basic "FormulaClave", limit: 2147483647
    t.varchar    "Formato",      limit: 50
  end

  create_table "tx_message_cellphones", primary_key: "Id", force: :cascade do |t|
    t.integer "schedule_messageid"
    t.varchar "name",               limit: 500
    t.varchar "cellpone",           limit: 50
  end

  create_table "tx_message_mails", force: :cascade do |t|
    t.integer "schedule_messageid"
    t.varchar "name",               limit: 500
    t.varchar "mail",               limit: 50
  end

  create_table "tx_schedule_messages", force: :cascade do |t|
    t.integer "schedule_id"
    t.integer "Num_Alarma"
    t.integer "letter_color"
    t.integer "background_color"
    t.varchar "message",          limit: 500
    t.integer "bit_date",         limit: 2
    t.integer "bit_description",  limit: 2
    t.integer "bit_escala",       limit: 2
    t.integer "bit_limit",        limit: 2
    t.integer "bit_reference",    limit: 2
  end

  create_table "tx_schedule_patrons", force: :cascade do |t|
    t.integer "schedule_id"
    t.integer "order_number"
    t.integer "field_formatid"
    t.varchar "values",         limit: 500
    t.varchar "operator",       limit: 45
    t.varchar "amount",         limit: 45
  end

  create_table "tx_schedule_references", force: :cascade do |t|
    t.integer "schedule_id"
    t.integer "field_formatId"
    t.integer "key_id"
    t.integer "session_id"
  end

  create_table "tx_sessions", primary_key: "Id", force: :cascade do |t|
    t.integer "Lay_id"
    t.integer "Format_id"
    t.varchar "Description",  limit: 500
    t.integer "Sessioncolor"
    t.integer "Interval"
    t.varchar "Inactive",     limit: 1
    t.varchar "Update",       limit: 1
    t.integer "Sessiontype",  limit: 2
  end

  create_table "txfilterxusr", primary_key: "Id", force: :cascade do |t|
    t.integer "Txfiltro_id", limit: 2
    t.integer "Prioridad"
    t.integer "IdUsuario"
  end

  create_table "user_work_group_spaces", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "groupColumn_id"
    t.integer  "typeChart"
    t.integer  "area"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "user_work_spaces", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "porcentual_session_id"
    t.integer  "area"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                            default: "", null: false
    t.string   "encrypted_password",               default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                  default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "password_changed_at"
    t.string   "unique_session_id",      limit: 1
    t.datetime "last_activity_at"
    t.datetime "expired_at"
    t.string   "user_name"
    t.string   "name"
    t.string   "last_name"
    t.date     "birthday"
    t.string   "boss_name"
    t.string   "phone"
    t.string   "ext"
    t.integer  "profile_id"
    t.string   "area_id"
    t.string   "employment"
    t.integer  "activo"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "habilitado"
  end

  create_table "views", force: :cascade do |t|
    t.string   "name"
    t.integer  "crear"
    t.integer  "editar"
    t.integer  "eliminar"
    t.integer  "leer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
