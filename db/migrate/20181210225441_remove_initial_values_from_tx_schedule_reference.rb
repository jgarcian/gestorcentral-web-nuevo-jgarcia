class RemoveInitialValuesFromTxScheduleReference < ActiveRecord::Migration[5.0]
  def change
    remove_column :tx_schedule_references, :Filter_id, :integer
  end
end
