class AddKeyidToTxFilterDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :tx_filter_details, :key_id, :int
    add_column :tx_filter_details, :token_id, :string
  end
end
