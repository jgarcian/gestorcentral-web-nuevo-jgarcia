class AddScheduleToGestorSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :gestor_settings, :day_start, :string
    add_column :gestor_settings, :day_end, :string
    add_column :gestor_settings, :night_start, :string
    add_column :gestor_settings, :night_end, :string
  end
end
